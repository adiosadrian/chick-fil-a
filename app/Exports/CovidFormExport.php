<?php

namespace App\Exports;

use App\Models\CovidForm;
use Maatwebsite\Excel\Concerns\FromCollection;

class CovidFormExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return CovidForm::all();
    }
}
