<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ActiveUser;
use Validator;
use Auth;
use DB;
use Symfony\Component\HttpFoundation\Response;
use App\Rules\MatchOldPassword;
use File;
use Active;
use Illuminate\Support\Facades\Hash;
use App\Events\LikeEvent;
use Carbon\Carbon;
class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index( Request $request )
    {
        $key = \Request::get('q');
        $isLoggedIn = \Request::get('isLoggedIn');

        if($_GET){
            if($key && $isLoggedIn == "All" ){
                $user = User::where('firstname','LIKE',"%{$key}%")
                ->orWhere('lastname','LIKE',"%{$key}%")
                ->orWhere('user_type','LIKE',"%{$key}%")
                ->orWhere('email','LIKE',"%{$key}%")
                ->latest()->paginate(10);
            }
            elseif($isLoggedIn == 0 ){
                $user = User::where('firstname','LIKE',"%{$key}%")
                ->orWhere('lastname','LIKE',"%{$key}%")
                ->orWhere('user_type','LIKE',"%{$key}%")
                ->orWhere('email','LIKE',"%{$key}%")
                ->orderBy('isLoggedIn','DESC')
                ->latest()->paginate(10);
            }
            else{
                $user = User::where('firstname','LIKE',"%{$key}%")
                ->orWhere('lastname','LIKE',"%{$key}%")
                ->orWhere('user_type','LIKE',"%{$key}%")
                ->orWhere('email','LIKE',"%{$key}%")
                ->orderBy('isLoggedIn','ASC')
                ->latest()->paginate(10);
            }

            
        }
        else{
            $user = User::latest()->paginate(10);                                     
        }
        
        return response()->json([
            'data' => $user            
        ]);        
             
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function search_user_by_key()
    {
    	$key = \Request::get('q');
        $unit = User::where('firstname','LIKE',"%{$key}%")
                ->orWhere('lastname','LIKE',"%{$key}%")
                ->orWhere('user_type','LIKE',"%{$key}%")
                ->orWhere('email','LIKE',"%{$key}%")
                ->with('active_user')->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = User::find($id);
          
        // if (is_null($user)) {            
        //     return response('User does not exist',401);
        // }
        // $response=[
        //     'user'=>$user            
        // ];        
        // return response($response,201);     

        $user = User::find($id);
        return response()->json($user);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'firstname'     =>'required|string',            
            'lastname'      =>'required|string',
            'phone'         =>'required|string',                   
            'email'         =>'required|unique:users,email,'.$id

        ]);

        if ($validator->fails()) {
            // return response(['error' => $validator->errors(), 'Validation Error']);          
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }
        $user = User::find($id);     
                    
        if($request['profile_filename']){                  
            $image = $request->file('profile_filename');            
            $file_name = time().'.'.$image->extension(); //Custom Filename

            $image->move(public_path('profiles'), $file_name);
            $destinationPath = 'profiles/';
            $file_path = $destinationPath.$file_name;  
            
            if($user->profile_filename){
                File::delete($destinationPath.'/'.$user->profile_filename);     
            }
                    
        }            
            
            $user->firstname           = $request->get('firstname');    
            $user->lastname            = $request->get('lastname');    
            $user->email               = $request['email'];
            $user->phone               = $request['phone'];            
            $user->date_of_birth       = $request['date_of_birth'] != 'null' ? $request['date_of_birth'] : NULL;
            $user->date_hired          = $request['date_hired'] != 'null' ? $request['date_hired'] : NULL;
            $user->address             = $request['address'] != 'null' ? $request['address'] : NULL;
            $user->city                = $request['city'] != 'null' ? $request['city'] : NULL;
            $user->state               = $request['state'] != 'null' ? $request['state'] : NULL;
            $user->zip                 = $request['zip'] != 'null' ? $request['zip'] : NULL;
            $user->facebook_link       = $request['facebook_link'] != 'null' ? $request['facebook_link'] : NULL;
            $user->instagram_link      = $request['instagram_link'] != 'null' ? $request['instagram_link'] : NULL;
            $user->google_link         = $request['google_link'] != 'null' ? $request['google_link'] : NULL;
            $user->profile_filename    = $request['profile_filename'] ? $file_name : NULL;
            $user->profile_path        = $request['profile_filename'] ? $file_path : NULL;            

            $user->save(); 
        $res = [
            'success' => true,   
            'data' =>  $user,            
            'message' => 'User Updated!'
        ];
        return response()->json($res, 201);
        
    }

    public function update_user(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'firstname'     =>'required|string',
            'lastname'      =>'required|string',
            'location'      =>'required|string',                        
            'email'         =>'required|unique:users,email,'.$id

        ]);

        if ($validator->fails()) {
            // return response(['error' => $validator->errors(), 'Validation Error']);          
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }            
        $user = User::find($id);                
        $user->firstname           = $request->get('firstname');    
        $user->lastname            = $request->get('lastname');    
        $user->email               = $request['email'];
        $user->phone               = $request['phone'] != 'null' ? $request['phone'] : NULL;
        $user->location            = $request['location'] != 'null' ? $request['location'] : NULL;
        $user->address             = $request['address'] != 'null' ? $request['address'] : NULL;
        $user->date_of_birth       = $request['date_of_birth'] != 'null' ? $request['date_of_birth'] : NULL;
        $user->date_hired          = $request['date_hired'] != 'null' ? $request['date_hired'] : NULL;
        $user->user_type           = $request['user_type'] != 'null' ? $request['user_type'] : NULL;
        $user->city                = $request['city'] != 'null' ? $request['city'] : NULL;
        $user->state               = $request['state'] != 'null' ? $request['state'] : NULL;
        $user->zip                 = $request['zip'] != 'null' ? $request['zip'] : NULL;    
        $user->save(); 
        
        $admins = User::where('user_type','Admin')->get();
        foreach($admins as $admin){
        $details = [
                'greeting' => 'Hi',
                'body' => 'An employee has updated his/her account.',
                'thanks' => 'Thanks!',
        ];
        // $admin->notify(new \App\Notifications\EmployeeUpdateNotification($details));
        }

        $res = [
            'success' => true,   
            'data' =>  $user,            
            'message' => 'User Updated!'
        ];
        return response()->json($res, 201);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();     
        $res = [            
            'data'    => $user,
            'message' => 'User Successfully deleted'
        ];   
        return response()->json($res, 201);
    }
    public function register(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'file'          =>'mimes:jpeg,png,bmp,tiff,jpg',
            'firstname'     =>'required|string',
            'lastname'      =>'required|string',
            'phone'         =>'required|string',
            'user_type'     =>'required|string',    
            'location'      =>'required|string',                    
            'email'         =>'required|string|unique:users,email',
        ]);

        if ($validator->fails()) {
            // return response(['error' => $validator->errors(), 'Validation Error']);          
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }

        if($request['file']) {
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads/profile_pics', $fileName, 'public');

            $file_path = '/storage/' . $filePath;
            $file_name = time().'_'.$request->file->getClientOriginalName();             
        }            
        
        $user=User::create([
            'firstname'             =>$request['firstname'],            
            'lastname'              =>$request['lastname'],            
            'email'                 =>$request['email'],
            'phone'                 =>$request['phone'],
            'address'               =>$request['address'] != 'null' ? $request['address'] : NULL,
            'date_of_birth'         =>$request['date_of_birth'] != 'null' ? $request['date_of_birth'] : NULL,
            'date_hired'            =>$request['date_hired'] != 'null' ? $request['date_hired'] : NULL,
            'city'                  =>$request['city'] != 'null' ? $request['city'] : NULL,
            'state'                 =>$request['state'] != 'null' ? $request['state'] : NULL,
            'location'              =>$request['location'] != 'null' ? $request['location'] : NULL,
            'zip'                   =>$request['zip'] != 'null' ? $request['zip'] : NULL,                       
            'profile_filename'      =>$request['file'] ? $file_name : NULL ,
            'profile_path'          =>$request['file'] ? $file_path : NULL,
            'password'              =>bcrypt("password"),
            'user_type'             =>$request['user_type']

        ]);   
             

        $token=$user->createToken('myapptoken')->plainTextToken;

        DB::table('password_resets')->insert([
            'email' => $user['email'],
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        
        $details = [
            'greeting' => 'Hi',
            'firstname' =>$request['firstname'],
            'lastname'  =>$request['lastname'],
            'email'     =>$request['email'],            
            'body'      => 'Admin added a new post',
            'thanks'    => 'Thanks!',
        ];

        $user->notify(new \App\Notifications\NewUserNotification($details,$token));  
        
        $response=[
            'user'=>$user,
            'token'=>$token,
            'message' => 'User Successfully Created'
        ];
        
        return response($response, 201);

    }

    public function signin(Request $request)
    {
        
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
            'message' => 'Invalid login details'
                       ], 401);
        }            
            $user = User::where('email', $request['email'])->firstOrFail();
            $user->isLoggedIn        = 0;     
            $user->last_login_at = Carbon::now();               
            $user->save(); 
            $token = $user->createToken('auth_token')->plainTextToken;       
            // broadcast(new LikeEvent());                       
            // $user->touch();    
            return response()->json([
                       'access_token' => $token,
                       'user' => $user,
                       'token_type' => 'Bearer',
                       'message' => 'Successfully Login',
                       
            ]);
    }
    public function signout(Request $request)
    {
        auth()->user()->tokens()->delete();
        // DB::table('active_users')->where('user_id', '=', Auth::id())->delete();
        User::find(auth()->user()->id)->update(['isLoggedIn'=> '-1']);
        // broadcast(new LikeEvent());         
        return response()->json([
            'message' => 'Logged Out',
        ],201);

    }
    public function profile(Request $request)
    {
        
        return $request->user();
    }

    public function me(Request $request)
    {
        return response()->json([
            'data' => $request->user(),
        ]);
    }
    public function change_password(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($data, [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        if ($validator->fails()) {             
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        return response()->json([
            'message' => 'Successfull',
        ]);
        
    }

    public function set_password(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($data, [                        
            'password' => 'required|confirmed',
            'token' =>'required'
        ],
        [
            'token.required'=> 'Code is Required', // custom message             
        ]);
        $tokenData = DB::table('password_resets')->where('token', $request['token'])->first();

        if ($validator->fails()) {             
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }
        
        if(!$tokenData){
            return response()->json(['message' => 'The Token is Expired/Mismatch', 'Validation Error'], 422);  
        }

        User::where('email', $request['email'])->update(['password'=> Hash::make($request['password'])]);

        DB::table('password_resets')->where('email', $request['email'])->delete();
        return response()->json([
            'message' => 'Password Successfully Set',
        ]);
        
    }
    public function resend_set_password(Request $request, $id)
    {
        $user = User::find($id);

        $token=$user->createToken('myapptoken')->plainTextToken;

        DB::table('password_resets')->insert([
            'email' => $user['email'],
            'token' => $token,
            'created_at' => Carbon::now()
        ]);


        $details = [
            'greeting' => 'Hi',
            'firstname' =>$user['firstname'],
            'lastname'  =>$user['lastname'],
            'email'     =>$user['email'],            
            'body'      => 'Successfully Sent',
            'thanks'    => 'Thanks!',
        ];

        $user->notify(new \App\Notifications\NewUserNotification($details,$token));  
        
        $response=[
            'user'=>$user,
            'token'=>$token,
            'message' => 'Successfull'
        ];
        
        return response($response, 201);


    }
}

    
