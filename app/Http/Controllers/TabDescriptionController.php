<?php

namespace App\Http\Controllers;

use App\Models\TabDescription;
use Illuminate\Http\Request;
use Validator;
class TabDescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TabDescription  $tabDescription
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tab_description = TabDescription::find($id);
        return response()->json($tab_description);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TabDescription  $tabDescription
     * @return \Illuminate\Http\Response
     */
    public function edit(TabDescription $tabDescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TabDescription  $tabDescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'tab_description'       => 'required',                        
        ]);

        if ($validator->fails()) {
            return response(['message' => $validator->errors(), 'Validation Error'],422);            
        }        

        $tab_description = TabDescription::find($id);            
        $tab_description->tab_description  = $request->get('tab_description');
        $tab_description->save();

        $res = [
            'success' => true,
            'data'    => $tab_description,
            'message' => 'TabDescription Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TabDescription  $tabDescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(TabDescription $tabDescription)
    {
        //
    }
}
