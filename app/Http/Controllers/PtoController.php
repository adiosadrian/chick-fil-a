<?php

namespace App\Http\Controllers;

use App\Models\Pto;
use App\Models\User;
use App\Models\TabDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Events\LikeEvent;
use Validator;
use Auth;
class PtoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {        
        $key = \Request::get('q');

        if($key){
            if(!in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $ptos = Pto::where('description','LIKE',"%{$key}%")                
                ->where('user_id','=',$request->user()->id)
                ->where('type','=','pto')
                ->with('user')->has('user')->latest()->paginate(5);
                
            }
            else{
                $ptos = Pto::where('name','LIKE',"%{$key}%")              
                ->where('type','=','pto')               
                ->with('user')->has('user')->latest()->paginate(5);
            }
        }
        else{
            if(!in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $ptos = Pto::where('user_id',$request->user()->id)
                ->where('type','=','pto')
                ->with('user')->has('user')->latest()->paginate(5);
                
            }
            else{
                $ptos = Pto::where('type','=','pto')->with('user')->has('user')->latest()->paginate(5);
            }
            
        }
        $tab_description = TabDescription::where('tab_name','=','pto')->first();
        // $employee = User::where('id','!=', $request->user()->id)->orderBy('lastname')->get();
        $employee = User::orderBy('lastname')->get();
        return response()->json([
            'data'            => $ptos,
            'employee'        => $employee,
            'tab_description' => $tab_description,
        ]);        
        return response()->json($ptos);        
    }

    public function employee_pto( Request $request )
    {
        $ptos = Pto::where('user_id',$request['id'])->with('user')->has('user')->latest()->paginate(5);
        
        return response()->json([
            'data' => $ptos,
            
        ]);        
        return response()->json($ptos);        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $validator = Validator::make($data, [            
            'start_date'     => 'required|date',
            'end_date'       => 'required|date',            
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }                                
        $pto = Pto::create($data);
        $subject = $request['type'] == 'pto' ? 'Pto Status' : 'Vacation Request Status';
        $details = [
            'greeting'      => 'Hi',
            'body'          => 'Employee requested a ' . ucwords($request['type']),
            'thanks'        => 'Thanks!',
            'name'          =>$request['name'],
            'description'   =>$request['description'],
            'approved'      =>$request['approved'],
            'start_date'    =>$request['start_date'],
            'end_date'      =>$request['end_date'],
            'total_hours'   =>$request['total_hours'],
            'type'          =>$request['type'],
            'subject'       =>$subject
        ];        
        Notification::route('mail', 'humanresources@cfaeastlex.net')->notify(new \App\Notifications\PtoNotification($details));  
        
        $res = [
            'success' => true,
            'data'    => $pto,
            'message' => 'Pto Uploaded'
        ];
        return response()->json($res, 201);                        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pto  $pto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $pto = Pto::with('user')->has('user')->find($id);
        return response()->json($pto);  

        
    }
    public function get_pto($id)
    {
        $pto = Pto::with('user')->has('user')->find($id);
        return response()->json($pto);    
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pto  $pto
     * @return \Illuminate\Http\Response
     */
    public function edit(Pto $pto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pto  $pto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [            
            'start_date'       => 'required|date',
            'end_date'       => 'required|date',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);            
        }

        $pto = Pto::find($id);   

        $pto->fill($data);

        // if ($pto->isDirty('approved')) {
        //     $user = User::find($pto->user_id);
        //     $details = [
        //         'greeting'      => 'Hi',
        //         'body'          => ucwords($pto['type']) . ' Status',
        //         'thanks'        => 'Thanks!',
        //         'name'          =>$pto['name'],
        //         'type'          =>$pto['type'],
        //         'approved'      =>$request['approved'],
        //         'start_date'    =>$request['start_date'],
        //         'end_date'      =>$request['end_date'],
        //         'itemPosted'    =>"Your request has been".' '. $request['approved'],
        //         'itemLink'      =>'/'.$pto['type'],
        //     ];            

        //     $user->notify(new \App\Notifications\PtoApprovedDeclineNotification($details));
        // }
        $pto->save();

        if(!empty($request['message'])){
            $user = User::find($pto->user_id);
            $details = [
                'greeting'      => 'Hi',
                'subject'       => ucwords($pto['type']) . ' Status',
                'body'          => $request['message'],
                'thanks'        => 'Thanks!',
                'name'          =>$pto['name'],
                'type'          =>$pto['type'],
                'approved'      =>$request['approved'],
                'start_date'    =>$request['start_date'],
                'end_date'      =>$request['end_date'],
                'itemPosted'    =>"Your request has been".' '. $request['approved'],
                'itemLink'      =>'/'.$pto['type'],
            ];            

            $user->notify(new \App\Notifications\PtoApprovedDeclineNotification($details));
        }
        broadcast(new LikeEvent());

        $res = [
            'success' => true,
            'data'    => $pto,
            'message' => 'Pto Updated!'            
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pto  $pto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pto = Pto::find($id);                        
        $pto->delete();        
        $response=[
            'success' => true,
            'message'=> "Pto Deleted!",
            'data' => $pto
        ];      
        return response($response,201);
    }
}
