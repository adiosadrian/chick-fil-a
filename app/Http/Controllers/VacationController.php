<?php

namespace App\Http\Controllers;
use App\Models\Pto;
use App\Models\User;
use App\Models\TabDescription;
use Illuminate\Http\Request;
use App\Events\LikeEvent;
use Validator;
use Auth;
class VacationController extends Controller
{
    public function index( Request $request )
    {        
        $key = \Request::get('q');

        if($key){
            if(!in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $ptos = Pto::where('description','LIKE',"%{$key}%")                
                ->where('user_id','=',$request->user()->id)
                ->where('type','=','vacation')
                ->with('user')->has('user')->latest()->paginate(5);
                
            }
            else{
                $ptos = Pto::where('name','LIKE',"%{$key}%")
                ->where('type','=','vacation')
                ->with('user')->has('user')->latest()->paginate(5);
            }
        }
        else{
            if(!in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $ptos = Pto::where('user_id',$request->user()->id)
                ->where('type','=','vacation')
                ->with('user')->has('user')->latest()->paginate(5);
                
            }
            else{
                $ptos = Pto::where('type','=','vacation')->with('user')->has('user')->latest()->paginate(5);
                
            }
            
        }
        $tab_description = TabDescription::where('tab_name','=','vacations')->first();
        // $employee = User::where('id','!=', $request->user()->id)->orderBy('lastname')->get();
        $employee = User::orderBy('lastname')->get();
        return response()->json([
            'data'            => $ptos,
            'employee'        => $employee,
            'tab_description' => $tab_description,
        ]);          
    }
}
