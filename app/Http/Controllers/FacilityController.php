<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\TabDescription;
use App\Models\FacilityReply;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use Auth;
use File;
use Illuminate\Support\Facades\Notification;
class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');
        $location = \Request::get('location');
        if($_GET){

            if(in_array($request->user()->user_type , array('Admin','Facility Manager')) &&  $location == "All"){
                $facilities = Facility::where('title','LIKE',"%{$key}%")->with('creator')->has('creator')->latest()->paginate(5);
            }
            elseif (in_array($request->user()->user_type , array('Admin','Facility Manager')) &&  $location != "All"){
                $facilities = Facility::where('title','LIKE',"%{$key}%")->where('location','LIKE',"%{$location}%")->with('creator')->has('creator')->latest()->paginate(5);
            }
            else{
                $facilities = Facility::where('user_id',$request->user()->id)->where('title','LIKE',"%{$key}%")->with('creator')->has('creator')->latest()->paginate(5);
            }
        }
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $facilities = Facility::with('creator')->has('creator')->latest()->paginate(5);
            }
            else{
                $facilities = Facility::where('user_id',$request->user()->id)->with('creator')->has('creator')->latest()->paginate(5);
            }
            
        }
        
        $tab_description = TabDescription::where('tab_name','=','facilities')->first();
        return response()->json([
            'data' => $facilities,
            'tab_description' => $tab_description,
        ]); 
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        
        $files=[];
        $file_name = '';        
        if($request['file']){     
            foreach ($request->file('file') as $media) {     
                                         
                // $file_name = Str::random(10).time().'.'.$media->extension();
                $file_name = preg_replace('/\s+/', '', $media->getClientOriginalName());    
                $media->move(public_path('facilities'), $file_name);
                $destinationPath = 'facilities/';
                $files[] = $file_name;  
            }
            
        }    


        $facility = Facility::create([            
            'user_id'     =>$request['user_id'],            
            'title'       =>$request['title'],
            'location'    =>$request['location'],
            'filename'    =>$files ? implode(',',$files) : NULL,
            'message'     =>$request['message']
        ]);        

        $details = [
                'greeting'      => 'Hi',                
                'thanks'        => 'Thanks!',
                'firstname'     => $request->user()->firstname,
                'lastname'      => $request->user()->lastname,
                'file'          =>$files ? implode(',',$files) : NULL,
                'facility_id'   => $facility->id,
                'created_at'    => $facility->created_at,
                'title'         => $request['title'],
                'location'      => $request['location'],
                'message'       => $request['message']
        ];        
        //Notification::route('mail', 'facilities@cfaeastlex.net')->notify(new \App\Notifications\FacilityNotification($details));
        Notification::route('mail', ['01591@chick-fil-a.com','03825@chick-fil-a.com'])->notify(new \App\Notifications\FacilityNotification($details));           
                
        $res = [
            'success' => true,
            'data'    => $facility,
            'message' => 'Your Form has been published!'
        ];
        return response()->json($res, 201);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $facility = Facility::find($id);
        return response()->json($facility);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'message'       => 'required',
            'title'      => 'required',
            'location'      => 'required',
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $facility = Facility::find($id);
        
        
        $destinationPath = 'facilities/';
        $thread_body = explode(",", $facility->filename);

        if($request->hasFile('file')) {                                  
                 
            $files=[];
            
            foreach ($request->file('file') as $media) {                
                
                $namewithextension = $media->getClientOriginalName();
                $name = explode('.', $namewithextension)[0]; // Filename 'filename'

                
                $filename  = Str::random(10).time().'.'.$media->extension();

                

                $media->move($destinationPath, $filename);                                                
                $files[] = $filename;                
            }            
    
            $facility->filename   = implode(',',$files);
            $facility->message   = $request->get('message');
            $facility->location   = $request->get('location');
            $facility->title     = $request->get('title');        
            $facility->save();  

            foreach ($thread_body as $key => $body) {
                $file_old = $destinationPath.$body;
                if(File::exists($file_old)){
    
                    File::delete($file_old);            
                }
            }     

        }

        else{
            $facility->message   = $request->get('message');
            $facility->title     = $request->get('title');    
            $facility->location     = $request->get('location');        
            $facility->save();
        }

        $res = [
            'success' => true,
            'data'    => $facility,
            'message' => 'Facility Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facilities = Facility::find($id);                                
        $facilities->delete();   

        $response=[
            'success' => true,
            'message'=> "Form Deleted!",
            'data' => $facilities
        ];      
        return response($response,201);
    }
}
