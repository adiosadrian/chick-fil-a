<?php

namespace App\Http\Controllers;

use App\Models\Uniform;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use Auth;
use File;

class UniformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $uniforms = Uniform::with('user')->has('user')->latest()->paginate(5);
        return response()->json([
            'data' => $uniforms            
        ]);        
        return response()->json($uniforms);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function latest()
    {
        
        $uniforms = Uniform::latest()->take(10)->get();
        
    	return response()->json($uniforms);
        
    }    
    public function search_uniform_by_key()
    {
    	$key = \Request::get('q');
        $unit = Uniform::where('uniform_title','LIKE',"%{$key}%")->with('user')->has('user')->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'                  => 'required|mimes:pdf,docx,doc',
            // 'category_id'           => 'required',
            'uniform_title'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){     
            $uniform = $request->file('file');            
            $file_name = time().'.'.$uniform->extension(); //Custom Filename

            $uniform->move(public_path('uniforms'), $file_name);
            $destinationPath = 'uniforms/';
            $file_path = $destinationPath.$file_name;   
            
            $uniform = Uniform::create([
                // 'category_id'           =>$request['category_id'],
                'uniform_title'         =>$request['uniform_title'],
                'uniform_filename'      =>$file_name,
                'uniform_path'          =>$file_path,
                'uniform_description'   =>$request['uniform_description'],
                'uploaded_by'           =>$request['uploaded_by']           
            ]);       

            $res = [
                'success' => true,
                'data'    => $uniform,
                'message' => 'Uniform Uploaded'
            ];
            return response()->json($res, 201);

        }           
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Uniform  $uniform
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $uniform = Uniform::find($id);
        return response()->json($uniform);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Uniform  $uniform
     * @return \Illuminate\Http\Response
     */
    public function edit(Uniform $uniform)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Uniform  $uniform
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'                  => 'required|mimes:pdf,docx,doc',
            // 'category_id'           => 'required',
            'uniform_title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $uniform = $request->file('file');
            // $filename = Str::random(40) . '.' . $video->extension(); //Custom Filename
            $file_name = time().'.'.$uniform->extension(); //Custom Filename

            $uniform->move(public_path('uniforms'), $file_name);
            $destinationPath = 'uniforms/';
            $file_path = $destinationPath.$file_name;         

            $uniform = Uniform::find($id);
            File::delete($destinationPath.'/'.$uniform->uniform_filename);    

            // $uniform->category_id           = $request->get('category_id');        
            $uniform->uniform_title         = $request->get('uniform_title');
            $uniform->uniform_filename      = $file_name;
            $uniform->uniform_path          = $file_path;
            $uniform->uniform_description   = $request->get('uniform_description');
            $uniform->save();

        }
        $res = [
            'success' => true,
            'data'    => $uniform,
            'message' => 'Uniform Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Uniform  $uniform
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $uniform = Uniform::find($id);
        $destinationPath = 'uniforms/';
        File::delete($destinationPath.'/'.$uniform->uniform_filename);
        $uniform->delete();          
        $response=[
            'success' => true,
            'message'=> "Uniform Deleted!",
            'data' => $uniform
        ];      
        return response($response,201);
    }
    public function download($file){                        
        $pathToFile = public_path('uniforms/'.$file);      
        $headers = array(
            'Content-Type: application/pdf',
          );

        return response()->download($pathToFile);        
        // return Response::download($pathToFile, $file, $headers);


    }
}
