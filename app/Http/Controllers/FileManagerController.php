<?php

namespace App\Http\Controllers;
use App\Models\TabDescription;
use App\Models\FileManager;
use Illuminate\Http\Request;
use Validator;
use Auth;
use File;
class FileManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');
        if($key){
            $file_manager = FileManager::where('title','LIKE',"%{$key}%")->with('user')->has('user')->latest()->paginate(5);
        }
        else{
            $file_manager = FileManager::with('user')->has('user')->latest()->paginate(5);    
        }
        $tab_description = TabDescription::where('tab_name','=','file_manager')->first();
        return response()->json([
            'data' => $file_manager,
            'tab_description' => $tab_description,
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [                     
            'title'    => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){     
            $file_manager = $request->file('file');            
            $file_name = time().'.'.$file_manager->extension();

            $file_manager->move(public_path('file_managers'), $file_name);
            $destinationPath = 'file_managers/';
            $file_path = $destinationPath.$file_name;   
            
            $file_manager = FileManager::create([                
                'title'        =>$request['title'],                
                'filename'     =>$file_name,
                'path'         =>$file_path,
                'description'  =>$request['description'],           
                'uploaded_by'  =>$request->user()->id,           
            ]);                   

        }         
        $res = [
            'success' => true,
            'data'    => $file_manager,
            'message' => 'File Uploaded'
        ];
        return response()->json($res, 201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file_manager = FileManager::find($id);
        return response()->json($file_manager);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function edit(FileManager $fileManager)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $file_manager = $request->file('file');
            $file_name = time().'.'.$file_manager->extension(); //Custom Filename

            $file_manager->move(public_path('file_managers'), $file_name);
            $destinationPath = 'file_managers/';
            $file_path = $destinationPath.$file_name;         

            $file_manager = FileManager::find($id);
            File::delete($destinationPath.'/'.$file_manager->filename);                
            $file_manager->title         = $request->get('title');
            $file_manager->filename      = $file_name;
            $file_manager->path          = $file_path;
            $file_manager->description   = $request->get('description');
            $file_manager->save();

        }
        $res = [
            'success' => true,
            'data'    => $file_manager,
            'message' => 'File Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FileManager  $fileManager
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file_manager = FileManager::find($id);
        
        $destinationPath = 'file_managers/';

        File::delete($destinationPath.'/'.$file_manager->filename);

        $file_manager->delete();          
        $response=[
            'success' => true,
            'message'=> "File Deleted!",
            'data' => $file_manager
        ];      
        return response($response,201);
    }
}
