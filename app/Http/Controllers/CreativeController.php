<?php

namespace App\Http\Controllers;

use App\Models\Creative;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use Auth;
use File;
class CreativeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $creatives = Creative::with('user')->latest()->paginate(5);
        return response()->json([
            'data' => $creatives            
        ]);        
        return response()->json($creatives);        
    }
    public function latest()
    {
        
        $creatives = Creative::latest()->take(10)->get();
        
    	return response()->json($creatives);
        
    }   
    public function search_creative_by_key()
    {
    	$key = \Request::get('q');
        $unit = Creative::where('creative_title','LIKE',"%{$key}%")->with('user')->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [
            'file'              => 'required|mimes:jpeg,png,bmp,tiff,jpg',
            // 'category_id'       => 'required',
            'creative_title'       => 'required|string'            
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){            
            $creative = $request->file('file');
            // $filename = Str::random(40) . '.' . $creative->extension(); //Custom Filename
            $file_name = time().'.'.$creative->extension(); //Custom Filename

            $creative->move(public_path('creatives'), $file_name);
            $destinationPath = 'creatives/';
            $file_path = $destinationPath.$file_name;                                         

            $creative = Creative::create([
                // 'category_id'       =>$request['category_id'],
                'creative_title'       =>$request['creative_title'],
                'creative_filename'    =>$file_name,
                'creative_path'        =>$file_path,
                'creative_description' =>$request['creative_description'],                
            ]);   

            $res = [
                'success' => true,
                'data'    => $creative,
                'message' => 'Creative Uploaded'
            ];
            return response()->json($res, 201);    

        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Creative  $creative
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $creative = Creative::find($id);
        return response()->json($creative); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Creative  $creative
     * @return \Illuminate\Http\Response
     */
    public function edit(Creative $creative)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Creative  $creative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'              => 'required|mimes:jpeg,png,bmp,tiff,jpg',
            // 'category_id'       => 'required',
            'creative_title'       => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
 
    
        if($request['file']){                       
            $creative = $request->file('file');            
            $file_name = time().'.'.$creative->extension(); //Custom Filename

            $creative->move(public_path('creatives'), $file_name);
            $destinationPath = 'creatives/';
            $file_path = $destinationPath.$file_name;  

            $creative = Creative::find($id);                                                               
            File::delete($destinationPath.'/'.$creative->creative_filename);                
        
            // $creative->category_id            = $request->get('category_id');
            $creative->creative_title         = $request->get('creative_title');
            $creative->creative_filename         = $file_name;
            $creative->creative_path         = $file_path;
            $creative->creative_description   = $request->get('creative_description');
            $creative->save();        
        }   
                    
        $res = [
            'success' => true,
            'data'    => $creative,
            'message' => 'Creative Uploaded'
        ];   
        return response()->json($res, 201);    


       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Creative  $creative
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $creative = Creative::find($id);                
        $destinationPath = 'creatives/';
        File::delete($destinationPath.'/'.$creative->creative_filename);
        $creative->delete();        
        $response=[
            'success' => true,
            'message'=> "Creative Deleted!",
            'data' => $creative
        ];      
        return response($response,201);
    }

    public function download($file){                        
        $pathToFile = storage_path('app/public/uploads/creatives/' .$file);        
        return response()->download($pathToFile);                
    }
}
