<?php

namespace App\Http\Controllers;
use App\Models\TrainingCategory;
use App\Models\ActiveUser;
use Illuminate\Http\Request;
use Validator;
use Carbon\Carbon;
class TrainingCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user_sessions = ActiveUser::where('created_at', '<', Carbon::now()->subMinutes(30)->toDateTimeString())->get();
        return response()->json([
            'data' => $user_sessions            
        ]);   
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'category_title' => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $training_category = TrainingCategory::create([
            'category_title'=>$request['category_title'],
            'category_description'=>$request['category_description']
        ]);        
        $res = [
            'success' => true,
            'data'    => $training_category,
            'message' => 'Training Category Created'
        ];
        return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingCategory  $trainingCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training_category = TrainingCategory::find($id);
          
        if (is_null($training_category)) {            
            return response('Training Category does not exist',404);
        }
        $response=[
            'success' => true,
            'training_category'=>$training_category            
        ];        
        return response()->json($response, 200);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingCategory  $trainingCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainingCategory $trainingCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingCategory  $trainingCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrainingCategory $trainingCategory)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'category_title' => 'required'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        $training_category = TrainingCategory::find($id);
        $training_category->category_title         = $request->get('category_title');
        $training_category->category_description   = $request->get('category_description');
        $training_category->save();

        $res = [
            'success' => true,
            'data'    => $training_category,
            'message' => 'TrainingCategory Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingCategory  $trainingCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training_category = TrainingCategory::find($id);
        $training_category->delete();        
        $response=[
            'success' => true,
            'message'=> "TrainingCategory Deleted!",
            'data' => $training_category
        ];      
        return response($response,201);
    }
}
