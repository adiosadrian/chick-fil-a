<?php

namespace App\Http\Controllers;

use App\Models\FacilityReply;
use App\Models\Facility;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Notification;
class FacilityReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $facility_reply = FacilityReply::where("facility_id", $id)->with('owner')->has('owner')->latest()->paginate(2);
        return response()->json([
            'data' => $facility_reply            
        ]);         
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'message' => 'required'
        ]);
        $reply = FacilityReply::create([                   
            'message'     => request('message'),
            'user_id'     => $request->user()->id,
            'facility_id' => request('facility_id'),
        ]);


        $facility = Facility::where('id',$request['facility_id'])->with('creator')->has('creator')->first();

        $details = [
            'greeting'      => 'Hi',                
            'thanks'        => 'Thanks!',
            'firstname'     => $request->user()->firstname,
            'lastname'      => $request->user()->lastname,            
            'created_at'    => $facility['created_at'],
            'facility_id'   => $facility['id'],
            'message'       => $request['message']
        ];        
        Notification::route('mail', $facility->creator->email)->notify(new \App\Notifications\FacilityReplyNotification($details));

        $res = [
            'success' => true,            
            'message' => 'Your reply has been sent',            
        ];
        return response()->json($res, 201);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FacilityReply  $facilityReply
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reply = FacilityReply::find($id);
        return response()->json($reply);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FacilityReply  $facilityReply
     * @return \Illuminate\Http\Response
     */
    public function edit(FacilityReply $facilityReply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FacilityReply  $facilityReply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'message'       => 'required'      
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $reply = FacilityReply::find($id);
        $reply->update($request->all());


        $res = [
            'success' => true,
            'data'    => $reply,
            'message' => 'Reply Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FacilityReply  $facilityReply
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = FacilityReply::find($id);                                
        $reply->delete();        
        $response=[
            'success' => true,
            'message'=> "Reply Deleted!",
            'data' => $reply
        ];      
        return response($response,201);
    }
}
