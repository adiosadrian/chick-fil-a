<?php

namespace App\Http\Controllers;

use App\Models\ReplyReaction;
use Illuminate\Http\Request;

class ReplyReactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ReplyReaction  $replyReaction
     * @return \Illuminate\Http\Response
     */
    public function show(ReplyReaction $replyReaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ReplyReaction  $replyReaction
     * @return \Illuminate\Http\Response
     */
    public function edit(ReplyReaction $replyReaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ReplyReaction  $replyReaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReplyReaction $replyReaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ReplyReaction  $replyReaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReplyReaction $replyReaction)
    {
        //
    }
}
