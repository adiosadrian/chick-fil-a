<?php

namespace App\Http\Controllers;

use App\Models\HrForm;
use App\Models\HrReply;
use App\Models\TabDescription;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Notification;

class HrFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hr_forms = HrForm::with('creator')->has('creator')->latest()->paginate(5);
        $tab_description = TabDescription::where('tab_name','=','hr_forms')->first();
        return response()->json([
            'data' => $hr_forms,
            'tab_description' => $tab_description,
        ]); 
    	
        
    }
    public function search_hr_forms_by_key()
    {
    	$key = \Request::get('q');
        $user_id = \Request::get('user_id');
        if($user_id){
            $unit = HrForm::where('title','LIKE',"%{$key}%")->where('user_id','=',$user_id)->with('creator')->has('creator')->latest()->paginate(10);
        }
        else{
            $unit = HrForm::where('title','LIKE',"%{$key}%")->with('creator')->has('creator')->latest()->paginate(10);
        }

        
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $hr_form = HrForm::create([            
            'user_id'     =>$request['user_id'],            
            'title'       =>$request['title'],
            'message'     =>$request['message']
        ]);        
        
        
        $from = User::find($request['user_id']);

        $details = [
                'greeting'      => 'Hi',                
                'thanks'        => 'Thanks!',
                'firstname'     => $from->firstname,
                'lastname'      => $from->lastname,
                'hr_form_id'    => $hr_form->id,
                'created_at'    => $hr_form->created_at,
                'title'         => $request['title'],
                'message'       => $request['message']
        ];

        
        Notification::route('mail', 'humanresources@cfaeastlex.net')->notify(new \App\Notifications\HrFormNotification($details));                
                
        $res = [
            'success' => true,
            'data'    => $hr_form,
            'message' => 'Your Form has been published!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrForm  $hrForm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hr_forms = HrForm::find($id);
        return response()->json($hr_forms);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HrForm  $hrForm
     * @return \Illuminate\Http\Response
     */
    public function edit(HrForm $hrForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrForm  $hrForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'message'       => 'required',
            'title'      => 'required'
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $hr_form = HrForm::find($id);
        $hr_form->message   = $request->get('message');
        $hr_form->title     = $request->get('title');
        $hr_form->status    = $request->get('status');
        $hr_form->save();

        $res = [
            'success' => true,
            'data'    => $hr_form,
            'message' => 'HrForm Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrForm  $hrForm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hr_forms = HrForm::find($id);                                
        $hr_forms->delete();   
        
        $replies = HrReply::where('hr_form_id',$hr_forms->id)->get();

        foreach($replies as $reply){
            $reply->delete();
        }

        $response=[
            'success' => true,
            'message'=> "Form Deleted!",
            'data' => $hr_forms
        ];      
        return response($response,201);
    }
    public function get_hr_form($id)
    {
        $hr_form = HrForm::latest()->with('creator')->has('creator')->where('user_id',$id)->paginate(5);   
        $tab_description = TabDescription::where('tab_name','=','hr_forms')->first();             
        return response()->json([
            'data'              => $hr_form,
            'tab_description'   => $tab_description            
        ]);        
        
    }
    public function user_hr_form( Request $request )
    {
        $hr_forms = HrForm::where('user_id',$request['id'])->with('creator')->has('creator')->latest()->paginate(5);
        $tab_description = TabDescription::where('tab_name','=','hr_forms')->first(); 
        return response()->json([
            'data' => $hr_forms,
            'tab_description'   => $tab_description
            
        ]);                  
    }

}
