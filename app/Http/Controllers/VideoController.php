<?php

namespace App\Http\Controllers;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;
use File;
use Illuminate\Support\Str;
use Twilio\Rest\Client;


class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        // $receiverNumber = "+639771008938";
        // $message = "This is testing";
  
        // try {
  
        //     $account_sid = getenv("TWILIO_SID");
        //     $auth_token = getenv("TWILIO_TOKEN");
        //     $twilio_number = getenv("TWILIO_FROM");
  
        //     $client = new Client($account_sid, $auth_token);
        //     $client->messages->create($receiverNumber, [
        //         'from' => $twilio_number, 
        //         'body' => $message]);
  
        //     dd('SMS Sent Successfully.');
  
        // } catch (Exception $e) {
        //     dd("Error: ". $e->getMessage());
        // }


        $videos = Video::latest()->paginate(5);
        return response()->json([
            'data' => $videos,     
        ]);        
           
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function search_video_by_key()
    {
    	$key = \Request::get('q');
        $unit = Video::where('video_title','LIKE',"%{$key}%")->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'              => 'mimes:mp4,mov,ogg,qt,MP4',
            // 'category_id'       => 'required|string',
            'video_title'       => 'required|string'         
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){            
            $video = $request->file('file');
            // $filename = Str::random(40) . '.' . $video->extension(); //Custom Filename
            $file_name = time().'.'.$video->extension(); //Custom Filename

            $video->move(public_path('videos'), $file_name);
            $destinationPath = 'videos/';
            $file_path = $destinationPath.$file_name;                                         

            $video = Video::create([
                // 'category_id'       =>$request['category_id'],
                'video_title'       =>$request['video_title'],
                'video_filename'    =>$file_name,
                'video_path'        =>$file_path,
                'video_description' =>$request['video_description'],                
            ]);   
            $res = [
                'success' => true,
                'data'    => $video,
                'message' => 'Video Uploaded'
            ];
            return response()->json($res, 201);    

        }           
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);
        return response()->json($video);
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'              => 'mimes:mp4,mov,ogg,qt',
            // 'category_id'       => 'required',
            'video_title'       => 'required|string'      
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $video = $request->file('file');
            // $filename = Str::random(40) . '.' . $video->extension(); //Custom Filename
            $file_name = time().'.'.$video->extension(); //Custom Filename

            $video->move(public_path('videos'), $file_name);
            $destinationPath = 'videos/';
            $file_path = $destinationPath.$file_name;         

            $video = Video::find($id);
            File::delete($destinationPath.'/'.$video->video_filename);    

            // $video->category_id         = $request->get('category_id');        
            $video->video_title         = $request->get('video_title');
            $video->video_filename      = $file_name;
            $video->video_path          = $file_path;
            $video->video_description   = $request->get('video_description');

            $video->save();

        }
        

        $res = [
            'success' => true,
            'data'    => $video,
            'message' => 'Video Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::find($id);                
        $destinationPath = 'videos/';
        // File::delete($destinationPath.'/'.$video->video_filename);
        $video->delete();        
        $response=[
            'success' => true,
            'message'=> "Video Deleted!",
            'data' => $video
        ];      
        return response($response,201);
    }
    public function download($file){                        
        $pathToFile = storage_path('app/public/uploads/videos/' .$file);        
        return response()->download($pathToFile);                
    }
}
