<?php 

namespace App\Http\Controllers;

use App\Models\Thread;

class ThreadSubscriptionsController extends Controller 
{
	public function store($channelId, Thread $thread)
	{
		$thread->subscribe();
        $response=[
            'success' => true,
            'message' => "Subscribe",
			'thread'=>$thread			
        ];        
        return response()->json($response, 201);            
	}

	public function destroy($channelId, Thread $thread)
	{
		$thread->unsubscribe();
		$response=[
            'success' => true,
			'message' => "Unsubscribe",
            'thread'=>$thread            
        ];        
        return response()->json($response, 201);         
	}
}