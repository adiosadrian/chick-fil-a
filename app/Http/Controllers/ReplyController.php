<?php

namespace App\Http\Controllers;
use App\Models\Thread;
use App\Models\Reply;
use App\Models\User;
use App\Models\ReplyReaction;
use Illuminate\Http\Request;
use Validator;
use App\Events\LikeEvent;
class ReplyController extends Controller
{

    public function index($id)
    {
        $reply = Reply::where("thread_id", $id)->with('owner')->has('owner')->latest()->paginate(2);
        $threads = Thread::where('id',$id)->with('creator')->has('creator')->first();
        return response()->json([
            'data' => $reply,         
            'threads' => $threads         
        ]); 
    	
        
    }

    public function reacted(Request $request)
    {
        $data = $request->all();  
        $reaction = array('reply_id' => $request['reply_id'],'reaction' => $request->react, 'user_id' => $request->user()->id);

        $reply = ReplyReaction::where('reply_id',$request['reply_id'])->where('user_id', $request->user()->id)->first();

        if ($reply) {            
            ReplyReaction::where('id',$reply->id)->update(['reaction' => $data['react']]);                
        }
        else{
            $react = ReplyReaction::create($reaction);

            $users = User::where('id','!=', $request->user()->id)->get();        
            $reply = Reply::with('thread')->find($react->reply_id);
            foreach($users as $user){
                $details = [
                    'greeting'      => 'Hi',
                    'body'          => $request->user()->firstname . ' reacted a comment',
                    'thanks'        => 'Thanks!',
                    'firstname'     => $request->user()->firstname,
                    'reply_id'      => $react['reply_id'],
                    'itemPosted'    => $request->user()->firstname . ' reacted a comment',
                    'itemLink'      => 'single_news'.'?id='.$reply->thread['id'],
                    'location'      => $reply->thread['location'] == "All" ? 'news' : $reply->thread['location'],
                    'created_at'    => $react['created_at'],                            
                    'title'         => $reply['title'],
                    'url'           => $reply->thread['location'] == "All" ? 'news' : $reply->thread['location'],
                             
                ];                     
                $user->notify(new \App\Notifications\ReplyReactNotification($details));
                // Artisan::call('queue:work');       
            }   
        }        
        broadcast(new LikeEvent());    
        $res = [
            'success' => true,
            'data'    => $data,
            'message' => 'Thread Updated!',
            'reply_id' => $reply
        ];
        return response()->json($res, 201);

    }
    public function unreacted($id)
    {
        if($id){      
                  
            $reaction = ReplyReaction::find($id);   
            $reaction->delete(); 
            $response=[
                'success' => true,
                'message'=> "Reaction Deleted!",
                'data' => $reaction
            ];  

            return response($response,201);
        }
    }


    public function get_visible_replies($id)
    {
        $reply = Reply::where('thread_id',$id)->where('active',0)->latest()->with('owner')->has('owner')->paginate(2); 
        $threads = Thread::where('id',$id)->with('creator')->has('creator')->first();               
        return response()->json([
            'data' => $reply,    
            'threads' => $threads                 
        ]);        
        
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'body' => 'required'
        ]);
        $thread = Reply::create([                   
        	'body' => request('body'),
        	'user_id' => request('user_id'),
            'thread_id' => request('thread_id'),
        ]);

        $res = [
            'success' => true,            
            'message' => 'Your reply has been sent'
        ];
        return response()->json($res, 201);        
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'body'       => 'required'      
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $reply = Reply::find($id);
        $reply->update($request->all());


        $res = [
            'success' => true,
            'data'    => $reply,
            'message' => 'Reply Updated!'
        ];
        return response()->json($res, 201);

    }
    public function destroy($id)
    {
        $reply = Reply::find($id);                                
        $reply->delete();        
        $response=[
            'success' => true,
            'message'=> "Reply Deleted!",
            'data' => $reply
        ];      
        return response($response,201);
    }

    public function show($id)
    {
        $reply = Reply::find($id);
        return response()->json($reply);
    }

}
