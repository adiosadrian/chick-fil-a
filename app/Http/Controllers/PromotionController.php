<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use App\Models\TabDescription;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;
use File;
class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        
        $key = \Request::get('q');
        $location = \Request::get('location');
        if($_GET){
            if($request->user()->location == 'All' &&  $location == "All" ){                      
                $promotion = Promotion::where('promotion_title','LIKE',"%{$key}%")->latest()->paginate(5);
            
            }  
            elseif($request->user()->location == 'All' &&  $location != "All" ){                      
                $promotion = Promotion::where('location',$location)->where('promotion_title','LIKE',"%{$key}%")->latest()->paginate(5);
            
            }          
            else{
                $promotion = Promotion::where('location',$request->user()->location)->where('promotion_title','LIKE',"%{$key}%")->latest()->paginate(5);
            }
                
        }            
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager')) || $request->user()->location == 'All'){               
                $promotion = Promotion::latest()->paginate(5);
            }
            else{
                $promotion = Promotion::where('location',$request->user()->location)->latest()->paginate(5);               
                
            }
        }

        $tab_description = TabDescription::where('tab_name','=','promotions')->first();
        return response()->json([
            'data' => $promotion,     
            'tab_description' => $tab_description,                      
        ]);                  
    }
    public function search_promotion_by_key()
    {
    	$key = \Request::get('q');
        $unit = Promotion::where('promotion_title','LIKE',"%{$key}%")->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $validator = Validator::make($data, [
            'promotion_title'       => 'required|string'         
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){            
            $promotion = $request->file('file');
            // $filename = Str::random(40) . '.' . $promotion->extension(); //Custom Filename
            $file_name = time().'.'.$promotion->extension(); //Custom Filename

            $promotion->move(public_path('promotions'), $file_name);
            $destinationPath = 'promotions/';
            $file_path = $destinationPath.$file_name;                                         

            $promotion = Promotion::create([                
                'promotion_title'       =>$request['promotion_title'],
                'promotion_filename'    =>$file_name,
                'promotion_path'        =>$file_path,
                'promotion_description' =>$request['promotion_description'],
                'promotion_type'        =>$request['promotion_type'],
                'location'              =>$request['location'],
            ]);   


            $res = [
                'success' => true,
                'data'    => $promotion,
                'message' => 'Promotion Uploaded'
            ];
            return response()->json($res, 201);    

        }           
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promotion = Promotion::find($id);
        return response()->json($promotion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'promotion_title'       => 'required|string'      
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $promotion = $request->file('file');
            // $filename = Str::random(40) . '.' . $promotion->extension(); //Custom Filename
            $file_name = time().'.'.$promotion->extension(); //Custom Filename

            $promotion->move(public_path('promotions'), $file_name);
            $destinationPath = 'promotions/';
            $file_path = $destinationPath.$file_name;         

            $promotion = Promotion::find($id);
            File::delete($destinationPath.'/'.$promotion->filename);    

               
            $promotion->promotion_title         = $request->get('promotion_title');
            $promotion->promotion_filename      = $file_name;
            $promotion->promotion_path          = $file_path;
            $promotion->promotion_description   = $request->get('promotion_description');
            $promotion->promotion_type          = $request->get('promotion_type');
            $promotion->location                = $request->get('location');
            
            $promotion->save();

        }
        

        $res = [
            'success' => true,
            'data'    => $promotion,
            'message' => 'Promotion Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promotion = Promotion::find($id);                
        $destinationPath = 'promotions/';
        File::delete($destinationPath.'/'.$promotion->filename);
        $promotion->delete();        
        $response=[
            'success' => true,
            'message'=> "Promotion Deleted!",
            'data' => $promotion
        ];      
        return response($response,201);
    }
}
