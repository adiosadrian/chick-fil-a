<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VideoCategory;
use Validator;
class VideoCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VideoCategory::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'category_title' => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $video_category = VideoCategory::create([
            'category_title'=>$request['category_title'],
            'category_description'=>$request['category_description']
        ]);        
        $res = [
            'success' => true,
            'data'    => $video_category,
            'message' => 'Video Category Created'
        ];
        return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video_category = VideoCategory::find($id);
          
        if (is_null($video_category)) {            
            return response('Video Category does not exist',404);
        }
        $response=[
            'success' => true,
            'video_category'=>$video_category            
        ];        
        return response()->json($response, 200);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'category_title' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        $video_category = VideoCategory::find($id);
        $video_category->category_title         = $request->get('category_title');
        $video_category->category_description   = $request->get('category_description');
        $video_category->save();

        $res = [
            'success' => true,
            'data'    => $video_category,
            'message' => 'Video Category Updated!'
        ];
        return response()->json($res, 201);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video_category = VideoCategory::find($id);
        $video_category->delete();        
        $response=[
            'success' => true,
            'message'=> "Video Category Deleted!",
            'data' => $video_category
        ];      
        return response($response,201);
    }
}
