<?php

namespace App\Http\Controllers;

use App\Models\CovidForm;
use App\Models\TabDescription;
use App\Models\User;
use Illuminate\Http\Request;
use App\Exports\CovidFormExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Notification;
use Validator;
use Auth;
use File;
class CovidFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');        

        if($key){         
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                
                $covid_forms = CovidForm::where('name','LIKE',"%{$key}%")
                ->orWhere('is_vaccinated','LIKE',"%{$key}%")
                ->with('user')->has('user')->latest()->paginate(5);                         
                
            }
            else{
                $covid_forms = CovidForm::where('name','LIKE',"%{$key}%")->where('user_id', $request->user()->id)->with('user')->has('user')->latest()->paginate(5);               
            }
        }
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                
                $covid_forms = CovidForm::with('user')->has('user')->latest()->paginate(5);                         
                
            }
            else{
                $covid_forms = CovidForm::where('user_id', $request->user()->id)->with('user')->has('user')->latest()->paginate(5);               
            }
        }

        $tab_description = TabDescription::where('tab_name','=','covid_forms')->first();
        return response()->json([
            'data' => $covid_forms,
            'tab_description' => $tab_description,
        ]);             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fullname = $request->user()->firstname . ' ' . $request->user()->lastname; 
        $data = $request->all();
        $validator = Validator::make($data, [            
            'is_vaccinated'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        $file_name = '';
        $file_path = '';
        if($request['file']){      
            $covid_form = $request->file('file');            
            $file_name = time().'.'.$covid_form->extension(); //Custom Filename

            $covid_form->move(public_path('covid_forms'), $file_name);             
            $destinationPath = getenv('MY_APP_URL')."/covid_forms/";
            $file_path = $destinationPath.$file_name;   
        }

        $covid_form = CovidForm::create([                
            'is_vaccinated' =>$request['is_vaccinated'],
            'filename'      =>$file_name ? $file_name : NULL,
            'full_path'     =>$file_path ? $file_path : NULL,
            'name'          =>$fullname,
            'user_id'       =>$request->user()->id,
            'location'      =>$request->user()->location,
        ]);      
        
        if($request['is_vaccinated'] == "yes"){
            $details = [
                'fullname'              => $fullname,
                'created_at'            => $covid_form->created_at,                
                'proof_of_vaccination'  => $file_path
            ];
            // Notification::route('mail', 'adrian@ad-ios.com')->notify(new \App\Notifications\CovidFormNotification($details));   
            // Notification::route('mail', 'humanresources@cfaeastlex.net')->notify(new \App\Notifications\CovidFormNotification($details));   
        }
            
        $res = [
            'success' => true,
            'data'    => $covid_form,
            'message' => 'Training Uploaded'
        ];
        return response()->json($res, 201);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CovidForm  $covidForm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $covid_form = CovidForm::find($id);
        return response()->json($covid_form);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CovidForm  $covidForm
     * @return \Illuminate\Http\Response
     */
    public function edit(CovidForm $covidForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CovidForm  $covidForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CovidForm $covidForm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CovidForm  $covidForm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $covid_form = CovidForm::find($id);
        $destinationPath = 'covid_forms/';
        File::delete($destinationPath.'/'.$covid_form->filename);
        $covid_form->delete();          
        $response=[
            'success' => true,
            'message'=> "Form Deleted!",
            'data' => $covid_form
        ];      
        return response($response,201);
    }
    public function export_data(Request $request) 
    {
        if($request['location'] == 'Richmond'){
            $records = CovidForm::where('location',$request['location'])->with('user')->has('user')->get();
        }
        elseif($request['location'] == 'Hamburg'){
            $records = CovidForm::where('location',$request['location'])->with('user')->has('user')->get();
        }
        else{
            $records = CovidForm::all();
        }
        

        // Excel::download($records, 'Covid-Form-List.xlsx'); 
        return response()->json([
            'data' => $records,            
        ]);  
    }

    public function fileExport() 
    {
        $records = CovidForm::all();
        return Excel::download($records, 'users-collection.xlsx');
    }   
    
}
