<?php

namespace App\Http\Controllers;

use App\Models\HrDoc;
use App\Models\TabDescription;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;
use File;
class HrDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hr_docs = HrDoc::with('user')->has('user')->latest()->paginate(5);
        $tab_description = TabDescription::where('tab_name','=','hr_doc')->first();
        return response()->json([
            'data' => $hr_docs,
            'tab_description' => $tab_description,          
        ]); 
    	return response()->json($hr_docs);
        
    }
    public function latest()
    {
        
        $hr_docs = HrDoc::latest()->take(10)->get();
        
    	return response()->json($hr_docs);
        
    }   
    public function search_hr_doc_by_key()
    {
    	$key = \Request::get('q');
        $unit = HrDoc::where('hr_doc_title','LIKE',"%{$key}%")->with('user')->has('user')->paginate(5);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [
            'hr_doc_title'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){     
            $hr_doc = $request->file('file');            
            $file_name = time().'.'.$hr_doc->extension();

            $hr_doc->move(public_path('hr_docs'), $file_name);
            $destinationPath = 'hr_docs/';
            $file_path = $destinationPath.$file_name;   
            
            $hr_doc = HrDoc::create([                
                'hr_doc_title'        =>$request['hr_doc_title'],
                'link'                =>$request['link'],
                'hr_doc_filename'     =>$file_name,
                'hr_doc_path'         =>$file_path,
                'hr_doc_description'  =>$request['hr_doc_description'],
                'uploaded_by'         =>$request['uploaded_by']           
            ]);                   

        }        
        else{
            $hr_doc = HrDoc::create([                
                'hr_doc_title'        =>$request['hr_doc_title'],
                'link'                =>$request['link'],
                'hr_doc_filename'     =>NULL,
                'hr_doc_path'         =>NULL,
                'hr_doc_description'  =>$request['hr_doc_description'],
                'uploaded_by'         =>$request['uploaded_by']           
            ]);     
        }   
        $res = [
            'success' => true,
            'data'    => $hr_doc,
            'message' => 'HrDoc Uploaded'
        ];
        return response()->json($res, 201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrDoc  $hrDoc
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hr_doc = HrDoc::find($id);
        return response()->json($hr_doc);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HrDoc  $hrDoc
     * @return \Illuminate\Http\Response
     */
    public function edit(HrDoc $hrDoc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrDoc  $hrDoc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'hr_doc_title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $hr_doc = $request->file('file');
            $file_name = time().'.'.$hr_doc->extension(); //Custom Filename

            $hr_doc->move(public_path('hr_docs'), $file_name);
            $destinationPath = 'hr_docs/';
            $file_path = $destinationPath.$file_name;         

            $hr_doc = HrDoc::find($id);
            File::delete($destinationPath.'/'.$hr_doc->hr_doc_filename);    
            $hr_doc->link                 = $request->get('link');
            $hr_doc->hr_doc_title         = $request->get('hr_doc_title');
            $hr_doc->hr_doc_filename      = $file_name;
            $hr_doc->hr_doc_path          = $file_path;
            $hr_doc->hr_doc_description   = $request->get('hr_doc_description');

            $hr_doc->save();

        }
        else{

            $hr_doc = HrDoc::find($id);            
            $hr_doc->hr_doc_title         = $request->get('hr_doc_title');
            $hr_doc->link                 = $request->get('link');
            $hr_doc->hr_doc_filename      = NULL;
            $hr_doc->hr_doc_path          = NULL;
            $hr_doc->hr_doc_description   = $request->get('hr_doc_description');

            $hr_doc->save();

        }

        $res = [
            'success' => true,
            'data'    => $hr_doc,
            'message' => 'HrDoc Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrDoc  $hrDoc
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hr_doc = HrDoc::find($id);
        $destinationPath = 'hr_docs/';
        File::delete($destinationPath.'/'.$hr_doc->hr_doc_filename);
        $hr_doc->delete();          
        $response=[
            'success' => true,
            'message'=> "HrDoc Deleted!",
            'data' => $hr_doc
        ];      
        return response($response,201);
    }
    public function download($file){                        
        $pathToFile = public_path('hr_docs/'.$file);      
        $headers = array(
            'Content-Type: application/pdf',
          );

        return response()->download($pathToFile);                
    }
}
