<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ActiveUser;
use Validator;
use Auth;
use DB;
class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {        
        
        $user = User::with('active_user')->get();
        // return response()->json([
        //     'data' => $user            
        // ]);
        // $data = User::paginate(10);
    	return response()->json($user);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = User::find($id);
          
        // if (is_null($user)) {            
        //     return response('User does not exist',401);
        // }
        // $response=[
        //     'user'=>$user            
        // ];        
        // return response($response,201);     

        $user = User::find($id);
        return response()->json($user);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name'          =>'required|string',  
            'mobile_number' =>'required|string',
            'date_of_birth' =>'required|date',
            'date_hired'    =>'required|date',
            'city'          =>'required|string',     
            'state'         =>'required|string',     
            'zip'           =>'required|string',                    
            'email'         =>'required|unique:users,email,'.$id

        ]);

        if ($validator->fails()) {
            // return response(['error' => $validator->errors(), 'Validation Error']);          
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }
        $user = User::find($id);                    
        if($request['profile_filename']){            
            $image = $request->get('profile_filename');
            $file_name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            $destinationPath = 'img/';
            \Image::make($request->get('profile_filename'))->save(public_path($destinationPath) . $file_name, 80);            
            $file_path = $destinationPath.$file_name;                         
        
            $user->name                = $request['name'];            
            $user->email               = $request['email'];
            $user->phone               = $request['phone'];
            $user->mobile_number       = $request['mobile_number'];
            $user->date_of_birth       = $request['date_of_birth'];
            $user->date_hired          = $request['date_hired'];
            $user->city                = $request['city'];
            $user->state               = $request['state'];
            $user->zip                 = $request['zip'];                                
            $user->profile_filename    = $request['profile_filename'] ? $file_name : NULL;
            $user->profile_path        = $request['profile_filename'] ? $file_path : NULL;            

            $user->save(); 
        }
        else{            
            $user->name                = $request['name'];            
            $user->email               = $request['email'];
            $user->phone               = $request['phone'];
            $user->mobile_number       = $request['mobile_number'];
            $user->date_of_birth       = $request['date_of_birth'];
            $user->date_hired          = $request['date_hired'];
            $user->city                = $request['city'];
            $user->state               = $request['state'];
            $user->zip                 = $request['zip'];                                
            $user->save(); 

        }

        

        
         

        $admins = User::where('isAdmin',0)->get();
        foreach($admins as $admin){
        $details = [
                'greeting' => 'Hi',
                'body' => 'An employee has updated his/her account.',
                'thanks' => 'Thanks!',
        ];
        // $admin->notify(new \App\Notifications\EmployeeUpdateNotification($details));
        }

        $res = [
            'success' => true,            
            'message' => 'User Updated!'
        ];
        return response()->json($res, 201);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();     
        $res = [            
            'data'    => $user,
            'message' => 'User Successfully deleted'
        ];   
        return response()->json($res, 201);
    }
    public function register(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'file'          =>'mimes:jpeg,png,bmp,tiff,jpg',
            'name'          =>'required|string',  
            'mobile_number' =>'required|string',
            'date_of_birth' =>'required|date',
            'date_hired'    =>'required|date',
            'city'          =>'required|string',     
            'state'         =>'required|string',     
            'zip'           =>'required|string', 
            'isadmin'       =>'required|string',        
            'username'      =>'required|string|unique:users,username',
            'email'         =>'required|string|unique:users,email',
            'password'      =>'required|string|confirmed|min:8'
        ]);

        if ($validator->fails()) {
            // return response(['error' => $validator->errors(), 'Validation Error']);          
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }

        if($request['file']) {
            $fileName = time().'_'.$request->file->getClientOriginalName();
            $filePath = $request->file('file')->storeAs('uploads/profile_pics', $fileName, 'public');

            $file_path = '/storage/' . $filePath;
            $file_name = time().'_'.$request->file->getClientOriginalName();             
        }                       
        $user=User::create([
            'name'                  =>$request['name'],
            'username'              =>$request['username'],
            'email'                 =>$request['email'],
            'phone'                 =>$request['phone'],
            'mobile_number'         =>$request['mobile_number'],
            'date_of_birth'         =>$request['date_of_birth'],
            'date_hired'            =>$request['date_hired'],
            'city'                  =>$request['city'],
            'state'                 =>$request['state'],
            'zip'                   =>$request['zip'],                                
            'profile_filename'      =>$request['file'] ? $file_name : NULL ,
            'profile_path'          =>$request['file'] ? $file_path : NULL,
            'password'              =>bcrypt($request['password']),
            // 'isAdmin'               =>$request['email_notification'] ? '1' : '0',
            'isadmin'               =>$request['isadmin']

        ]);              
        $token=$user->createToken('myapptoken')->plainTextToken;
        $response=[
            'user'=>$user,
            'token'=>$token,
            'message' => 'User Successfully Created'
        ];
        
        return response($response, 201);

    }

    public function signin(Request $request)
    {
        
        if (!Auth::attempt($request->only('username', 'password'))) {
            return response()->json([
            'message' => 'Invalid login details'
                       ], 401);
                   }            
            $user = User::where('username', $request['username'])->firstOrFail();
            
            $token = $user->createToken('auth_token')->plainTextToken;
            DB::table('active_users')->insert(array('user_id' =>   Auth::id()));


            return response()->json([
                       'access_token' => $token,
                       'user' => $user,
                       'token_type' => 'Bearer',
                       'message' => 'Successfully Login',
                       
            ]);
    }
    public function signout()
    {
        auth()->user()->tokens()->delete();
        DB::table('active_users')->where('user_id', '=', Auth::id())->delete();

        return [
            'message' => 'Logged Out'
        ];
    }
    public function profile(Request $request)
    {
        
        return $request->user();
    }

    public function me(Request $request)
    {
        return response()->json([
            'data' => $request->user(),
        ]);
    }
}
