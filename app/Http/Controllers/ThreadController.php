<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Notification;
use App\Jobs\SendNewsfeedNotification;
use App\Jobs\PostReactNotification;
use App\Models\Thread;
use App\Models\Reply;
use App\Models\User;
use App\Models\Channel;
use App\Models\Activity;
use App\Models\TabDescription;
use App\Models\ThreadReaction;
use Illuminate\Http\Request;
use App\Filters\ThreadFilters;
use Illuminate\Support\Str;
use App\Events\LikeEvent;
use Validator;
use Auth;
use Twilio\Rest\Client;
use Artisan;
use DB;
use File;
use FFMpeg;
use FFMpeg\FFProbe;
use Storage;
class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth')->except(['index', 'show']);
    // }
    public function index(Request $request)
    {
        
        $key = \Request::get('q');

        $location = \Request::get('location');
        $is_new = \Request::get('read');
        
        if($key){               
            $threads = Thread::where('title','LIKE',"%{$key}%")->where('location',$location)->with('creator','reactions','replies')->latest()->paginate(5);
        }
        elseif($is_new ){
            $threads = Thread::where('location',$location)->whereRaw('FIND_IN_SET("'.$request->user()->id.'",is_read)')->with('creator','replies','reactions')->count();             

        }
        else{
            // $featured = Thread::where('is_featured','=','1')->where('location',$location)->with('creator')->has('creator')->latest()->paginate(5);       
            $threads = Thread::where('location',$location)->with('creator','replies','reactions')->latest()->paginate(5); 
        }

        if($request['location'] == "Hamburg"){
            $tab_description = TabDescription::where('tab_name','=','hamburg_news')->first();
        }
        elseif($request['location'] == "Richmond"){
            $tab_description = TabDescription::where('tab_name','=','richmond_news')->first();
        }
        else{
            $tab_description = TabDescription::where('tab_name','=','news')->first();
        }
        
        return response()->json([
            'data' => $threads,
            'tab_description' => $tab_description
        ]);     	
        
    }
    public function update_counter(Request $request){
        $threads = Thread::where('location','=',$request['location'])->whereRaw('FIND_IN_SET("'.$request->user()->id.'",is_read)')->with('creator','replies','reactions')->get();

        foreach ($threads as $key => $thread) {
            $arr = array_diff(explode(',',$thread['is_read']), array($request->user()->id));
            $output = implode(',', $arr);

            Thread::find($thread['id'])->update(['is_read' => $output]);

        }
        return response()->json([
            'data' => $threads,            
        ]); 

    }

    public function single_news(Request $request)
    {
        

        $threads = Thread::where('id',$request['id'])->with('creator','replies','reactions')->has('creator')->first();      
        
        
        $details = [
            'user_id'         =>$request->user()->id,
            'itemPosted'      =>'Viewed the thread',
            'threadId'        => $request['id'],     
            'location'        => $threads->location       
        ];  
        
        
        $request->user()->notify(new \App\Notifications\NewsfeedNotification($details)); 

        return response()->json([
            'data' => $threads,            
        ]);     	
        
    }
    public function featured(Request $request)
    {
        $location = \Request::get('location');

        if($location == 'Hamburg'){
            if (!in_array($request->user()->user_type , array('Admin','Facility Manager')) ){

                $featured = Thread::where('is_featured', '=', 1)
                ->where(function ($query) use ($request) {
                    $query->where('location',$request->user()->location);
                })->with('creator','replies','reactions')->latest()->get();                           
                // $featured = Thread::where('is_featured','=','1')->where('location',$request->user()->location)->orWhere('location','All')->with('creator')->has('creator')->latest()->get();
            }
            else{
                $featured = Thread::where('is_featured','=','1')->where('location','Hamburg')->with('creator','replies','reactions')->latest()->get();                
            }         
        }
        else{
            if (!in_array($request->user()->user_type , array('Admin','Facility Manager')) ){

                $featured = Thread::where('is_featured', '=', 1)
                ->where(function ($query) use ($request) {
                    $query->where('location',$request->user()->location);
                })->with('creator','replies','reactions')->latest()->get();                           
                // $featured = Thread::where('is_featured','=','1')->where('location',$request->user()->location)->orWhere('location','All')->with('creator')->has('creator')->latest()->get();
            }
            else{
                $featured = Thread::where('is_featured','=','1')->where('location','=','Richmond')->with('creator','replies','reactions')->latest()->get();                
            }
        }
                    
        return response()->json([
            'featured' => $featured,                
        ]);     	
        
    }
    public function search_news_by_key()
    {
    	$key = \Request::get('q');
        $unit = Thread::where('title','LIKE',"%{$key}%")->with('creator')->has('creator')->latest()->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'title' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $files=[];
        $file_name = '';
        $file_path = '';
        if($request['file']){                 
            foreach ($request->file('file') as $media) {                
                $destinationPath = 'threads/';
                $namewithextension = $media->getClientOriginalName();                
                
                $name = Str::random(10).time();
                $filename  = $name.'.'.$media->extension();

                Storage::disk('thread_files')->put($filename, file_get_contents($media));

                
                if($media->extension() == 'mov'){                                        
                    
                    FFMpeg::fromDisk('thread_files')
                    ->open($filename)
                    ->export()
                    ->toDisk('thread_files')
                    ->save($name.'.mp4');          

                    unlink(public_path('threads/'.$filename));
                    
                    $filename = $name.'.mp4';
                    
                }                        

                $files[] = $filename;  

            }                        
        }  
        
        
        $is_read = User::where('id','!=', $request->user()->id)->pluck('id')->implode(',');
        $thread = Thread::create([
            'user_id'     =>$request['user_id'],           
            'is_read'     =>$is_read,            
            'title'       =>$request['title'],
            'body'        =>$files ? implode(',',$files) : NULL,
            'location'    =>$request['location'],
            'is_sent'     =>$request->user()->user_type != "Admin" ?  1 : 0,
            'send_email'  =>$request->user()->user_type != "Admin" ?  1 : $request['send_email'],
            'send_sms'    =>$request->user()->user_type != "Admin" ?  1 : $request['send_sms'],       
        ]);      
               
        // if($thread['location'] == "All"){
        //     $users = User::where('id','!=', $request->user()->id)->get();                    
        // }
        // else{
            $users = User::where('id','!=', $request->user()->id)->where('location','=', $request['location'])->orWhere('location','All')->get();
        // }

        $user_list = [];
        $details = [
            'greeting'      => 'Hi',
            'user_type'     => $thread->creator['user_type'],
            'body'          => $thread->creator['firstname'] . ' added a new post',
            'thanks'        => 'Thanks!',                
            'thread_id'     => $thread['id'],
            'itemPosted'    => $thread->creator['firstname']  . ' added a new post',
            'itemLink'      => 'single_news'.'?id='.$thread['id'],
            'location'      => $thread['location'],
            'created_at'    => $thread['created_at'],                            
            'title'         => $thread['title'],
            'url'           => $thread['location'] == "All" ? 'news' : $thread['location'],
            'send_email'    =>$request->user()->user_type != "Admin" ?  1 : $request['send_email'],
            'send_sms'      =>$request->user()->user_type != "Admin" ?  1 : $request['send_sms'],      
            
        ];

        SendNewsfeedNotification::dispatch($details,$users);  
        broadcast(new LikeEvent());  
        $res = [
            'success' => true,
            'data'    => $data,
            'message' => 'Your thread has been published!'
        ];
        return response()->json($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $threads = Thread::find($id);
        return response()->json($threads);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'body'       => 'required',
            'title'      => 'required'
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $thread = Thread::find($id);
        $destinationPath = 'threads/';
        $thread_body = explode(",", $thread->body);
         

        if($request->hasFile('file')) {                                  
                 
            $files=[];
            
            foreach ($request->file('file') as $media) {                
                
                $namewithextension = $media->getClientOriginalName();
                $name = explode('.', $namewithextension)[0]; // Filename 'filename'

                
                $filename  = Str::random(10).time().'.'.$media->extension();

                

                $media->move($destinationPath, $filename);                                                
                $files[] = $filename;                
            }            
    
            $thread->body   = implode(',',$files);
            $thread->title  = $request->get('title');
            $thread->save();  

            foreach ($thread_body as $key => $body) {
                $file_old = $destinationPath.$body;
                if(File::exists($file_old)){
    
                    File::delete($file_old);            
                }
            }     

        }
        
        else{                       
            $thread->title  = $request->get('title');
            $thread->body  = $request->get('body');
            $thread->save();  

        }
    
        $res = [
            'success' => true,
            'data'    => $thread,            
            'message' => 'Thread Updated!'
        ];
        return response()->json($res, 201);

    }

    public function reacted(Request $request)
    {
        $data = $request->all();  
        $reaction = array('thread_id' => $request['thread_id'],'reaction' => $request->react, 'user_id' => $request->user()->id);

        $thread = ThreadReaction::where('thread_id',$request['thread_id'])->where('user_id', $request->user()->id)->first();

        if ($thread) {            
            ThreadReaction::where('id',$thread->id)->update(['reaction' => $data['react']]);                            
        }
        else{
            $react = ThreadReaction::create($reaction);            
             
            $thread = Thread::find($react->thread_id);

            // if($thread['location'] == "All"){
            //     $users = User::where('id','!=', $request->user()->id)->get();        
            // }
            // else{
                $users = User::where('id','!=', $request->user()->id)->where('location','=', $thread['location'])->orWhere('location','All')->get();
            // }

            $details = [
                'greeting'      => 'Hi',
                'body'          => $request->user()->firstname . ' reacted a post',
                'thanks'        => 'Thanks!',
                'firstname'     => $request->user()->firstname,
                'thread_id'     => $react['thread_id'],
                'itemPosted'    => $request->user()->firstname . ' reacted a post',
                'itemLink'      => 'single_news'.'?id='.$thread['id'],
                'location'      => $thread['location'] == "All" ? 'news' : $thread['location'],
                'created_at'    => $react['created_at'],                            
                'title'         => $thread['title'],
                'url'           => $thread['location'] == "All" ? 'news' : $thread['location'],
                            
            ];                                  
            PostReactNotification::dispatch($details,$users); 
            broadcast(new LikeEvent());    
        }      
        
        $res = [
            'success' => true,
            'data'    => $data,
            'message' => 'Thread Updated!',            
            'thread_id' => $thread
        ];
        return response()->json($res, 201);

    }
    public function unreacted($id)
    {
        if($id){      
                  
            $reaction = ThreadReaction::find($id);   
            $reaction->delete(); 
            $response=[
                'success' => true,
                'message'=> "Reaction Deleted!",
                'data' => $reaction
            ];  

            return response($response,201);
        }
          

            
        
    }

    public function favorited(Request $request, $id)
    {
        $data = $request->all();

        $thread = Thread::find($id);
        $thread->is_featured  = "1";        
        $thread->save();
        $res = [
            'success' => true,
            'data'    => $thread,
            'message' => 'Thread Updated!'
        ];
        return response()->json($res, 201);

    }
    public function unfavorited(Request $request, $id)
    {
        $data = $request->all();

        $thread = Thread::find($id);
        $thread->is_featured  = "0";        
        $thread->save();
        $res = [
            'success' => true,
            'data'    => $thread,
            'message' => 'Thread Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $threads = Thread::find($id);
        
        $destinationPath = 'threads/';
        if($threads->body){
            $images = explode(",",$threads->body);
            foreach ($images as $key => $value) {
                File::delete($destinationPath.'/'.$value);
            }
        }                        
        $threads->delete();

        $response=[
            'success' => true,
            'message'=> "Thread Deleted!",
            'data' => $threads
        ];      
        return response($response,201);
    }
    public function getThreads(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);
        if($channel->exists){
            $threads->where('channel_id', $channel->id);
        }

        return $threads->get();
    }
}
