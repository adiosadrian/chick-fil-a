<?php

namespace App\Http\Controllers;

use App\Models\Training;
use App\Models\Activity;
use App\Models\User;
use App\Models\TabDescription;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;
use App\Events\LikeEvent;
use Validator;
use Auth;
use File;
class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');        
        $user_type = $request->user()->user_type;
        $location = $request->user()->location;

        if($key){         
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                
                $trainings = Training::where('training_title','LIKE',"%{$key}%")->with('user')->has('user')->latest()->paginate(5);                         
                
            }
            elseif($request->user()->location == "All"){
                $trainings = Training::where('training_title','LIKE',"%{$key}%")->whereRaw('FIND_IN_SET("'.$user_type.'",user_type)')->with('user')->has('user')->latest()->paginate(5);
            }
            else{
                $trainings = Training::where('training_title','LIKE',"%{$key}%")->whereRaw('FIND_IN_SET("'.$user_type.'",user_type)')->whereRaw('FIND_IN_SET("'.$location.'",location)')->with('user')->has('user')->latest()->paginate(5);               
            }
        }
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                
                $trainings = Training::with('user')->has('user')->latest()->paginate(5);                         
                
            }
            elseif($request->user()->location == "All"){
                $trainings = Training::whereRaw('FIND_IN_SET("'.$user_type.'",user_type)')->with('user')->has('user')->latest()->paginate(5);
            }
            else{
                $trainings = Training::whereRaw('FIND_IN_SET("'.$user_type.'",user_type)')->whereRaw('FIND_IN_SET("'.$location.'",location)')->with('user')->has('user')->latest()->paginate(5);               
            }
        }

        $tab_description = TabDescription::where('tab_name','=','trainings')->first();
        return response()->json([
            'data' => $trainings,
            'tab_description' => $tab_description,
        ]);     	
        
    }
    public function latest()
    {
        
        $trainings = Training::latest()->take(10)->get();
        
    	return response()->json($trainings);
        
    }    
    public function search_training_by_key()
    {
    	$key = \Request::get('q');
        $unit = Training::where('training_title','LIKE',"%{$key}%")->with('user')->has('user')->paginate(5);
        return response()->json(['unit' => $unit ]);    	
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'                  => 'required|mimes:pdf,docx,doc',
            // 'category_id'           => 'required',
            'training_title'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        $file_name = '';
        $file_path = '';
        if($request['file']){     
            $training = $request->file('file');            
            $file_name = time().'.'.$training->extension(); //Custom Filename
            $training->move(public_path('trainings'), $file_name);
            $destinationPath = 'trainings/';
            $file_path = $destinationPath.$file_name;               
                           
        }      
        $training = Training::create([            
            'training_title'        =>$request['training_title'],
            'link'                  =>$request['link'],
            'user_type'             =>$request['user_type'],
            'location'              =>$request['location'],
            'training_filename'     =>$file_name ? $file_name : NULL,
            'training_path'         =>$file_path ? $file_path : NULL,
            'training_description'  =>$request['training_description'],
            'uploaded_by'           =>$request['uploaded_by']           
        ]);    
        
        //Add activity log when training is uploaded
        $users = User::get();
        foreach($users as $user){   
            $details = [
                'user_id'         =>$user->id,
                'itemPosted'      =>'Admin uploaded a new training',
                'itemLink'        =>'training'
            ];                            
            $user->notify(new \App\Notifications\TrainingNotification($details));            
        }          
        broadcast(new LikeEvent());
        $res = [
            'success' => true,
            'data'    => $training,
            'message' => 'Training Uploaded'
        ];
        return response()->json($res, 201);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training = Training::find($id);
        return response()->json($training);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function edit(Training $training)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'                  => 'required|mimes:pdf,docx,doc',
            // 'category_id'           => 'required',
            'training_title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $file_name = "";
        $file_path = "";
        if($request['file']){                       
            $training = $request->file('file');
            $file_name = time().'.'.$training->extension(); //Custom Filename
            $training->move(public_path('trainings'), $file_name);
            $destinationPath = 'trainings/';
            $file_path = $destinationPath.$file_name;         
            $training = Training::find($id);
            
            if($training->training_filename){
                File::delete($destinationPath.'/'.$training->training_filename);     
            }

        }
        $training = Training::find($id);
        $training->link                   = $request->get('link') != 'null' ? $request->get('link'): Null;     
        $training->training_title         = $request->get('training_title');
        $training->user_type              = $request->get('user_type');
        $training->location               = $request->get('location');
        $training->training_title         = $request->get('training_title');
        $training->training_filename      = $file_name ? $file_name : NULL;
        $training->training_path          = $file_path ? $file_path : NULL;
        $training->training_description   = $request->get('training_description') != 'null' ? $request->get('training_description'): Null; 
        $training->save();

        $res = [
            'success' => true,
            'data'    => $training,
            'message' => 'Training Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training = Training::find($id);
        $destinationPath = 'trainings/';
        File::delete($destinationPath.'/'.$training->training_filename);
        $training->delete();          
        $response=[
            'success' => true,
            'message'=> "Training Deleted!",
            'data' => $training
        ];      
        return response($response,201);
    }

    public function download($file){                        
        $pathToFile = public_path('trainings/'.$file);      
        $headers = array(
            'Content-Type: application/pdf',
          );

        return response()->download($pathToFile);        
        // return Response::download($pathToFile, $file, $headers);


    }
}
