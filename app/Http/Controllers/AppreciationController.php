<?php

namespace App\Http\Controllers;

use App\Models\Appreciation;
use App\Models\TabDescription;
use App\Models\Activity;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use App\Events\LikeEvent;
use Validator;
use Auth;
use File;
class AppreciationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        
        $key = \Request::get('q');
        $location = \Request::get('location');
        if($_GET){
            
            if($request->user()->location == 'All' &&  $location == "All" ){                      
                $appreciation = Appreciation::where('appreciation_title','LIKE',"%{$key}%")->latest()->paginate(5);                
            
            }  
            elseif($request->user()->location == 'All' &&  $location != "All" ){                      
                // $appreciation = Appreciation::where('location',$location)->where('appreciation_title','LIKE',"%{$key}%")->latest()->paginate(5);                
                $appreciation = Appreciation::whereRaw('FIND_IN_SET("'.$location.'",location)')->where('appreciation_title','LIKE',"%{$key}%")->latest()->paginate(5);
            
            }          
            else{
                // $appreciation = Appreciation::where('location',$request->user()->location)->where('appreciation_title','LIKE',"%{$key}%")->latest()->paginate(5);                
                $appreciation = Appreciation::whereRaw('FIND_IN_SET("'.$request->user()->location.'",location)')->where('appreciation_title','LIKE',"%{$key}%")->latest()->paginate(5);                
            }
                
        }            
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager')) || $request->user()->location == 'All'){               
                $appreciation = Appreciation::latest()->paginate(5);                
            }
            else{
                // $appreciation = Appreciation::where('location',$request->user()->location)->latest()->paginate(5);               
                $appreciation = Appreciation::whereRaw('FIND_IN_SET("'.$request->user()->location.'",location)')->latest()->paginate(5);    
                
                
            }
        }

        $tab_description = TabDescription::where('tab_name','=','appreciations')->first();
        return response()->json([
            'data' => $appreciation,     
            'tab_description' => $tab_description,                          
        ]);                  
    }
    public function search_appreciation_by_key()
    {
    	$key = \Request::get('q');
        $unit = Appreciation::where('appreciation_title','LIKE',"%{$key}%")->paginate(10);
        return response()->json(['unit' => $unit ]);    	
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->all();
        $validator = Validator::make($data, [
            'appreciation_title'       => 'required|string'         
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){            
            $appreciation = $request->file('file');
            // $filename = Str::random(40) . '.' . $appreciation->extension(); //Custom Filename
            $file_name = time().'.'.$appreciation->extension(); //Custom Filename

            $appreciation->move(public_path('appreciations'), $file_name);
            $destinationPath = 'appreciations/';
            $file_path = $destinationPath.$file_name;                                         

            $appreciation = Appreciation::create([                
                'appreciation_title'       =>$request['appreciation_title'],
                'appreciation_filename'    =>$file_name,
                'appreciation_path'        =>$file_path,
                'appreciation_description' =>$request['appreciation_description'],
                'appreciation_type'        =>$request['appreciation_type'],
                'location'                 =>$request['location'],
            ]);   

            //Add activity log when appreciation is uploaded  
            $locations = explode(",",$request['location']);
            foreach($locations as $location){
                $users = User::where('location',$location)->where('user_type','!=','Admin')->get();

                foreach($users as $user){
                    $details = [
                        'user_id'         =>$user->id,
                        'itemPosted'      =>'Admin uploaded a new appreciation',
                        'itemLink'        =>'appreciation' 
                    ];                            
                    $user->notify(new \App\Notifications\AppreciationNotification($details));                    
                }
            }
            broadcast(new LikeEvent());
            
                
            $res = [
                'success' => true,
                'data'    => $appreciation,
                'message' => 'Appreciation Uploaded'
            ];
            return response()->json($res, 201);    

        }           
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Appreciation  $appreciation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appreciation = Appreciation::find($id);
        return response()->json($appreciation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Appreciation  $appreciation
     * @return \Illuminate\Http\Response
     */
    public function edit(Appreciation $appreciation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Appreciation  $appreciation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'appreciation_title'       => 'required|string'      
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $appreciation = $request->file('file');
            // $filename = Str::random(40) . '.' . $appreciation->extension(); //Custom Filename
            $file_name = time().'.'.$appreciation->extension(); //Custom Filename

            $appreciation->move(public_path('appreciations'), $file_name);
            $destinationPath = 'appreciations/';
            $file_path = $destinationPath.$file_name;         

            $appreciation = Appreciation::find($id);
            File::delete($destinationPath.'/'.$appreciation->filename);    

               
            $appreciation->appreciation_title         = $request->get('appreciation_title');
            $appreciation->location                   = $request->get('location');
            $appreciation->appreciation_filename      = $file_name;
            $appreciation->appreciation_path          = $file_path;
            $appreciation->appreciation_description   = $request->get('appreciation_description');
            $appreciation->appreciation_type          = $request->get('appreciation_type');

            $appreciation->save();

        }
        

        $res = [
            'success' => true,
            'data'    => $appreciation,
            'message' => 'Appreciation Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Appreciation  $appreciation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appreciation = Appreciation::find($id);                
        $destinationPath = 'appreciations/';
        File::delete($destinationPath.'/'.$appreciation->filename);
        $appreciation->delete();        
        $response=[
            'success' => true,
            'message'=> "Appreciation Deleted!",
            'data' => $appreciation
        ];      
        return response($response,201);
    }
}
