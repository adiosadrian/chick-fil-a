<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TabDescription;
use App\Models\Permission;
use App\Models\UserType;
use Validator;
class UserTypeController extends Controller
{
    public function index( Request $request )
    {        
        $key = \Request::get('q');

        if($key){
            $user_types = UserType::where('title','LIKE',"%{$key}%")->latest()->paginate(5);
        }
        else{
            $user_types = UserType::paginate(5);            
        }
        $tab_description = TabDescription::where('tab_name','=','pto')->first();
        
        return response()->json([
            'data'            => $user_types,            
            'tab_description' => $tab_description,
        ]);                    
    }
    public function store(Request $request)
    {
        
        $data = $request->all();
        $validator = Validator::make($data, [            
            'title'     => 'required|unique:user_types,title',                   
                   
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }                                
        $user_type = UserType::create($data);  

        $res = [
            'success' => true,
            'data'    => $user_type,               
        ];
        return response()->json($res, 201);                        
    }
    public function show($id)
    {
        
        $user_types = UserType::with('permission')->find($id);
        return response()->json($user_types);  

        
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [            
            'title'       => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);            
        }

        $user_type = UserType::find($id);   

        $user_type->fill($data);
        $user_type->save();        

        $res = [
            'success' => true,
            'data'    => $user_type,
            'message' => 'User Type Updated!'            
        ];
        return response()->json($res, 201);
    }
    public function update_permission(Request $request, $id)
    {
        $data = $request->all();
        // $validator = Validator::make($data, [            
        //     'title'       => 'required|string'            
        // ]);

        // if ($validator->fails()) {
        //     return response(['error' => $validator->errors(), 'Validation Error']);            
        // }
        $del = ',';
        $res = array_merge(explode ( $del , $request['user_access']  ),explode ( $del , $request['filemanager_access'] ),explode ( $del , $request['filemanager_access'] )); 
        $test = implode($del,$res);

        $user_access                = explode(",",$request['user_access']);
        // $filemanager_access         = explode(",",$request['filemanager_access']);
        // $message_access             = explode(",",$request['message_access']);
        // $vacation_access            = explode(",",$request['vacation_access']);
        // $facility_access            = explode(",",$request['facility_access']);
        // $tab_description_access     = explode(",",$request['tab_description_access']);

        // $merge_access = array_merge($user_access,$filemanager_access,$message_access,$vacation_access,$facility_access,$tab_description_access);
        $merge_access = implode(",",$user_access);

        $permission = Permission::updateOrCreate([            
            'role_id'   => $request['id'],
        ],[
            'role_id'   => $request['id'],
            'slug'      => $merge_access,               
            'name'      => "test"
        ]);


        // $permission = Permission::create([
        //     'role_id'   => $request['id'],
        //     'slug'      => $merge_access,               
        //     'name'      => "test"
        // ]);

        $res = [
            'success' => true,
            'data'    => $permission,
            'message' => 'User Type Updated!'
        ];
        return response()->json($res, 201);
    }

    public function destroy($id)
    {
        $user_type = UserType::find($id);                        
        $user_type->delete();        
        $response=[
            'success' => true,
            'message'=> "UserType Deleted!",
            'data' => $user_type
        ];      
        return response($response,201);
    }
}
