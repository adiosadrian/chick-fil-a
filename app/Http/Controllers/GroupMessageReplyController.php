<?php

namespace App\Http\Controllers;
use App\Models\GroupMessage;
use App\Models\Activity;
use App\Models\User;
use App\Models\GroupMessageReply;
use Illuminate\Http\Request;
use Validator;
class GroupMessageReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $group_message = GroupMessageReply::where("group_message_id", $id)->with('owner')->has('owner')->latest()->paginate(2);
        return response()->json([
            'data' => $group_message            
        ]);     	
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'message' => 'required'
        ]);
        
        $group_message = GroupMessage::where('id',$request['group_message_id'])->first();
        if(!in_array($request->user()->user_type , array('Admin','Facility Manager'))){
            
            $message_reply = GroupMessageReply::create([                   
                'message'           => $request['message'],
                'from'              => $request->user()->id,
                'to'                => $group_message->from,
                'group_message_id'  => $group_message->id
            ]);

        }
        else{            
            $message_reply = GroupMessageReply::create([                   
                'message'           => $request['message'],
                'from'              => $request->user()->id,
                'to'                => $group_message->to,
                'group_message_id'  => $group_message->id
            ]);

        }
        $res = [
            'success' => true,            
            'message' => 'Your reply has been sent'
        ];
        return response()->json($res, 201);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GroupMessageReply  $groupMessageReply
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reply = GroupMessageReply::find($id);
        return response()->json($reply);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GroupMessageReply  $groupMessageReply
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupMessageReply $groupMessageReply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GroupMessageReply  $groupMessageReply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupMessageReply $groupMessageReply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GroupMessageReply  $groupMessageReply
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = GroupMessageReply::find($id);                                
        $reply->delete();        
        $response=[
            'success' => true,
            'message'=> "Reply Deleted!",
            'data' => $reply
        ];      
        return response($response,201);
    }
}
