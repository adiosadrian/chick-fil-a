<?php

namespace App\Http\Controllers;

use App\Models\CreativeCategory;
use Illuminate\Http\Request;
use Validator;
class CreativeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return CreativeCategory::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'category_title' => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $creative_category = CreativeCategory::create([
            'category_title'        =>$request['category_title'],
            'category_description'  =>$request['category_description']
        ]);        
        $res = [
            'success' => true,
            'data'    => $creative_category,
            'message' => 'CreativeCategory Created'
        ];
        return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CreativeCategory  $creativeCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $creative_category = CreativeCategory::find($id);
          
        if (is_null($creative_category)) {            
            return response('Training Category does not exist',404);
        }
        $response=[
            'success' => true,
            'creative_category'=>$creative_category            
        ];        
        return response()->json($response, 200);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CreativeCategory  $creativeCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(CreativeCategory $creativeCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CreativeCategory  $creativeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreativeCategory $creativeCategory)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'category_title' => 'required'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        $creative_category = CreativeCategory::find($id);
        $creative_category->category_title         = $request->get('category_title');
        $creative_category->category_description   = $request->get('category_description');
        $creative_category->save();

        $res = [
            'success' => true,
            'data'    => $creative_category,
            'message' => 'CreativeCategory Updated!'
        ];
        return response()->json($res, 201);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CreativeCategory  $creativeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $creative_category = CreativeCategory::find($id);
        $creative_category->delete();        
        $response=[
            'success' => true,
            'message'=> "CreativeCategory Deleted!",
            'data' => $creative_category
        ];      
        return response($response,201);
    }
}
