<?php

namespace App\Http\Controllers;
use App\Models\HrForm;
use App\Models\HrReply;
use Illuminate\Http\Request;
use Validator;

class HrReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $hr_reply = HrReply::where("hr_form_id", $id)->with('owner')->has('owner')->latest()->paginate(2);
        return response()->json([
            'data' => $hr_reply            
        ]);     	
        
    }

    public function get_visible_replies($id)
    {
        $reply = HrReply::where('thread_id',$id)->where('active',0)->latest()->with('owner')->has('owner')->paginate(2);                
        return response()->json([
            'data' => $reply,            
        ]);        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'message' => 'required'
        ]);
        $hr_form = HrReply::create([                   
        	'message' => request('message'),
        	'user_id' => request('user_id'),
            'hr_form_id' => request('hr_form_id'),
        ]);

        $res = [
            'success' => true,            
            'message' => 'Your reply has been sent'
        ];
        return response()->json($res, 201);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HrReply  $hrReply
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hr_reply = HrReply::find($id);
        return response()->json($hr_reply);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HrReply  $hrReply
     * @return \Illuminate\Http\Response
     */
    public function edit(HrReply $hrReply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HrReply  $hrReply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'message'       => 'required'      
            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $hr_reply = HrReply::find($id);
        $hr_reply->update($request->all());


        $res = [
            'success' => true,
            'data'    => $hr_reply,
            'message' => 'Reply Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HrReply  $hrReply
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hr_reply = HrReply::find($id);                                
        $hr_reply->delete();        
        $response=[
            'success' => true,
            'message'=> "Reply Deleted!",
            'data' => $hr_reply
        ];      
        return response($response,201);
    }

    
}
