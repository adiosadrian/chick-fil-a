<?php

namespace App\Http\Controllers;

use App\Models\GroupMessage;
use App\Models\GroupMessageReply;
use App\Models\TabDescription;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Notification;
use Validator;

class GroupMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');

        if($key){

            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $group_messages = GroupMessage::where('title','LIKE',"%{$key}%")->where('from',$request->user()->id)->with('creator')->has('creator')->latest()->paginate(5);
            }
            else{
                $group_messages = GroupMessage::where('title','LIKE',"%{$key}%")->where('to',$request->user()->id)->with('creator')->has('creator')->latest()->paginate(5);
            }

        }
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){
                $group_messages = GroupMessage::where('from',$request->user()->id)->with('creator')->has('creator')->latest()->paginate(5);
            }
            else{
                $group_messages = GroupMessage::where('to',$request->user()->id)->with('creator')->has('creator')->latest()->paginate(5);
            }
            
        }
        
        $users = User::orderBy('lastname','ASC')->get();
        $tab_description = TabDescription::where('tab_name','=','group_message')->first();
        return response()->json([
            'data'            => $group_messages,
            'tab_description' => $tab_description,
            'users'           => $users,
        ]); 
    	
        
    }
    public function get_group_message( Request $request )
    {
        $group_messages = GroupMessage::where('to',$request['user_id'])->with('creator')->has('creator')->latest()->paginate(5);
        $tab_description = TabDescription::where('tab_name','=','group_message')->first();
        return response()->json([
            'data' => $group_messages,
            'tab_description' => $tab_description,            
        ]);                  
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title'     => 'required'
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }                     
        $locations = explode(',',$request['location']);
        
        $user_types= explode(',',$request['user_type']);     
        
        $users = User::whereIn('user_type', $user_types)->whereIn('location',$locations)->get();


        $files=[];
        $file_name = '';        
        if($request['file']){     

            foreach ($request->file('file') as $media) {     
                                         
                // $file_name = Str::random(10).time().'.'.$media->extension();
                $file_name = preg_replace('/\s+/', '', $media->getClientOriginalName());    
                $media->move(public_path('group_messages'), $file_name);
                $destinationPath = 'group_messages/';
                $files[] = $file_name;  
            }
            
        }    
        foreach($users as $user){

                 


            $group_message = GroupMessage::create([            
                'to'          =>$user->id,            
                'from'        =>$request->user()->id,
                'title'       =>$request['title'],  
                'file'        =>$files ? implode(',',$files) : NULL,
                'user_type'   =>$user->user_type,
                'location'    =>$user->location,
                'message'     =>$request['message'],
                'is_sent'     =>0,
                'send_email'  =>$request['send_email'],
                'send_sms'    =>$request['send_sms']
            ]);   
    
        }
            
        $res = [
            'success' => true,            
            'message' => 'Your Message has been sent!'
        ];
        return response()->json($res, 201);
    }

    public function sendMultipleUsers(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title'     => 'required'
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }                     
        $userIds = explode(',',$request['to']);
                
        
        $users = User::whereIn('id', $userIds)->get();

        $files=[];
        $file_name = '';        
        if($request['file']){     

            foreach ($request->file('file') as $media) {     
                                         
                // $file_name = Str::random(10).time().'.'.$media->extension();
                $file_name = preg_replace('/\s+/', '', $media->getClientOriginalName());


    
                $media->move(public_path('group_messages'), $file_name);
                $destinationPath = 'group_messages/';
                $files[] = $file_name;  
            }
            
        }    
        foreach($users as $user){               
            $group_message = GroupMessage::create([            
                'to'          =>$user->id,            
                'from'        =>$request->user()->id,
                'title'       =>$request['title'],       
                'file'        =>$files ? implode(',',$files) : NULL,     
                'user_type'   =>$user->user_type,
                'location'    =>$user->location,
                'message'     =>$request['message'],
                'is_sent'     =>0,
                'send_email'  =>0,
                'send_sms'    =>1
            ]);   

        }            
        $res = [
            'success' => true,            
            'message' => 'Your Message has been sent!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GroupMessage  $groupMessage
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group_messages = GroupMessage::find($id);
        return response()->json($group_messages);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GroupMessage  $groupMessage
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupMessage $groupMessage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GroupMessage  $groupMessage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupMessage $groupMessage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GroupMessage  $groupMessage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group_messages = GroupMessage::find($id);                
        $group_messages->delete();
        $response=[
            'success' => true,
            'message'=> "Form Deleted!",
            'data' => $group_messages
        ];      
        return response($response,201);
    }
    public function bulkDelete($id)
    {
        $ids = explode(",",$id);
        foreach ($ids as $key => $id) {
            $group_messages = GroupMessage::find($id);
            $group_messages->delete();
        }
        
        $response=[
            'success' => true,
            'message'=> "Data Deleted!", 
            
        ];      
        return response($response,201);

    }
}
