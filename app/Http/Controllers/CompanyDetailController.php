<?php

namespace App\Http\Controllers;

use App\Models\CompanyDetail;
use Illuminate\Http\Request;
use Validator;
use File;
class CompanyDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_detail = CompanyDetail::first();

        return response()->json([
            'data' => $company_detail            
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CompanyDetail  $companyDetail
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyDetail $companyDetail)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CompanyDetail  $companyDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyDetail $companyDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CompanyDetail  $companyDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyDetail $companyDetail)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'company_name'     =>'required|string',            
            'company_address'  =>'required|string',
            'company_phone'    =>'required|string',                   
            'company_email'    =>'required'

        ]);

        if ($validator->fails()) {              
            return response()->json(['message' => $validator->errors(), 'Validation Error'], 422);  
        }
        $company_detail = CompanyDetail::find($request['id']);     
                    
        if($request['filename']){                  
            $image = $request->file('filename');            
            $file_name = 'logo.'.$image->extension(); //Custom Filename

            $image->move(public_path('img'), $file_name);
            $destinationPath = 'img/';
            $file_path = $destinationPath.$file_name;  
            
            
                    
        }            
            
            $company_detail->company_name           = $request['company_name'];
            $company_detail->company_address        = $request['company_address'];
            $company_detail->company_email          = $request['company_email'];
            $company_detail->company_phone          = $request['company_phone'];                       
            $company_detail->company_facebook       = $request['company_facebook'] != 'null' ? $request['company_facebook'] : NULL;
            $company_detail->company_instagram      = $request['company_instagram'] != 'null' ? $request['company_instagram'] : NULL;            
            $company_detail->filename               = $request['filename'] ? $file_name : NULL;
            $company_detail->filepath               = $request['filename'] ? $file_path : NULL;            

            $company_detail->save(); 
        $res = [
            'success' => true,   
            'data' =>  $company_detail,            
            'message' => 'Company Detail Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CompanyDetail  $companyDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyDetail $companyDetail)
    {
        //
    }
}
