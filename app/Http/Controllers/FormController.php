<?php

namespace App\Http\Controllers;

use App\Models\Form;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TabDescription;
use App\Events\LikeEvent;
use Validator;
use Auth;
use File;
class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
        $key = \Request::get('q');        
        $user_type = $request->user()->user_type;
        $location = $request->user()->location;
        if($key){         
            $forms = Form::where('form_title','LIKE',"%{$key}%")->with('user')->has('user')->latest()->paginate(5);
        }
        else{
            $forms = Form::with('user')->has('user')->latest()->paginate(5);
        }
        $tab_description = TabDescription::where('tab_name','=','forms')->first();
        return response()->json([
            'data' => $forms,
            'tab_description' => $tab_description,
        ]); 
    	
        
    }
    public function latest()
    {
        
        $forms = Form::latest()->take(10)->get();
        
    	return response()->json($forms);
        
    }   
    public function search_form_by_key()
    {
    	$key = \Request::get('q');
        $unit = Form::where('form_title','LIKE',"%{$key}%")->with('user')->has('user')->paginate(5);
        return response()->json(['unit' => $unit ]);    	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [
            'form_title'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        $file_name = '';
        $file_path = '';
        if($request['file']){     
            $form = $request->file('file');            
            $file_name = time().'.'.$form->extension();

            $form->move(public_path('forms'), $file_name);
            $destinationPath = 'forms/';
            $file_path = $destinationPath.$file_name;                                    

        }        
        $form = Form::create([                
            'form_title'        =>$request['form_title'],
            'link'              =>$request['link'],
            'form_filename'     =>$file_name ? $file_name : NULL,
            'form_path'         =>$file_name ? $file_name : NULL,
            'form_description'  =>$request['form_description'],
            'uploaded_by'       =>$request->user()->id 
        ]);   
        broadcast(new LikeEvent());   
        $res = [
            'success' => true,
            'data'    => $form,
            'message' => 'Form Uploaded'
        ];
        return response()->json($res, 201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $form = Form::find($id);
        return response()->json($form);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            // 'file'                  => 'required|mimes:pdf,docx,doc',
            // 'category_id'           => 'required',
            'form_title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        $file_name = "";
        $file_path = "";

        if($request['file']){                       
            $form = $request->file('file');
            $file_name = time().'.'.$form->extension(); //Custom Filename

            $form->move(public_path('forms'), $file_name);
            $destinationPath = 'forms/';
            $file_path = $destinationPath.$file_name;         

            $form = Form::find($id);
            if($form->form_filename){
                File::delete($destinationPath.'/'.$form->form_filename);    
            }
        }
        $form = Form::find($id);
        $form->link               = $request->get('link') != 'null' ? $request->get('link'): Null;    
        $form->form_title         = $request->get('form_title') != 'null' ? $request->get('form_title'): Null;    
        $form->form_filename      = $file_name ? $file_name : NULL;
        $form->form_path          = $file_path ? $file_path : NULL;
        $form->form_description   = $request->get('form_description') != 'null' ? $request->get('form_description'): Null;    
        $form->save();

        $res = [
            'success' => true,
            'data'    => $form,
            'message' => 'Form Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::find($id);
        $destinationPath = 'forms/';
        File::delete($destinationPath.'/'.$form->form_filename);
        $form->delete();          
        $response=[
            'success' => true,
            'message'=> "Form Deleted!",
            'data' => $form
        ];      
        return response($response,201);
    }
    public function download($file){                        
        $pathToFile = public_path('forms/'.$file);      
        $headers = array(
            'Content-Type: application/pdf',
          );

        return response()->download($pathToFile);        
        // return Response::download($pathToFile, $file, $headers);


    }
}
