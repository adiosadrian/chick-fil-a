<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserNotificationsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(User $user)
	{
		$unreadNotifications = auth()->user()->unreadNotifications->filter(function ($notification) {
			return $notification->data['itemPosted'] !== 'Viewed the thread';
		});
		
		// return auth()->user()->unreadNotifications->where('');
	}

    public function destroy(User $user, $notificationId)
    {
    	auth()->user()->notifications()->findOrFail($notificationId)->markAsRead();
    }
}
