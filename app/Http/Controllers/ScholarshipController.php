<?php

namespace App\Http\Controllers;

use App\Models\Scholarship;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\TabDescription;
use Validator;
use Auth;
use File;
class ScholarshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $scholarships = Scholarship::with('user')->has('user')->latest()->paginate(5);
        $tab_description = TabDescription::where('tab_name','=','scholarship')->first();
        return response()->json([
            'data' => $scholarships,
            'tab_description' => $tab_description          
        ]);     	
        
    }
    public function latest()
    {
        
        $scholarships = Scholarship::latest()->take(10)->get();
        
    	return response()->json($scholarships);
        
    }   
    public function search_scholarship_by_key()
    {
    	$key = \Request::get('q');
        $unit = Scholarship::where('scholarship_title','LIKE',"%{$key}%")->with('user')->has('user')->paginate(5);
        return response()->json(['unit' => $unit ]);    	
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [
            'scholarship_title'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        if($request['file']){     
            $scholarship = $request->file('file');            
            $file_name = time().'.'.$scholarship->extension();

            $scholarship->move(public_path('scholarships'), $file_name);
            $destinationPath = 'scholarships/';
            $file_path = $destinationPath.$file_name;   
            
            $scholarship = Scholarship::create([                
                'scholarship_title'        =>$request['scholarship_title'],
                'link'                     =>$request['link'],
                'scholarship_filename'     =>$file_name,
                'scholarship_path'         =>$file_path,
                'scholarship_description'  =>$request['scholarship_description'],
                'uploaded_by'              =>$request['uploaded_by']           
            ]);                   

        }        
        else{
            $scholarship = Scholarship::create([                
                'scholarship_title'        =>$request['scholarship_title'],
                'link'                     =>$request['link'],
                'scholarship_filename'     =>NULL,
                'scholarship_path'         =>NULL,
                'scholarship_description'  =>$request['scholarship_description'],
                'uploaded_by'              =>$request['uploaded_by']           
            ]);     
        }   
        $res = [
            'success' => true,
            'data'    => $scholarship,
            'message' => 'Scholarship Uploaded'
        ];
        return response()->json($res, 201);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scholarship = Scholarship::find($id);
        return response()->json($scholarship);  
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function edit(Scholarship $scholarship)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'scholarship_title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }

        if($request['file']){                       
            $scholarship = $request->file('file');
            $file_name = time().'.'.$scholarship->extension(); //Custom Filename

            $scholarship->move(public_path('scholarships'), $file_name);
            $destinationPath = 'scholarships/';
            $file_path = $destinationPath.$file_name;         

            $scholarship = Scholarship::find($id);
            File::delete($destinationPath.'/'.$scholarship->scholarship_filename);    
            $scholarship->link                      = $request->get('link') != 'null' ? $request->get('link'): Null;     
            $scholarship->scholarship_title         = $request->get('scholarship_title') != 'null' ? $request->get('scholarship_title'): Null;     
            $scholarship->scholarship_filename      = $file_name;
            $scholarship->scholarship_path          = $file_path;
            $scholarship->scholarship_description   = $request->get('scholarship_description') != 'null' ? $request->get('scholarship_description'): Null;     

            $scholarship->save();

        }
        else{

            $scholarship = Scholarship::find($id);            
            $scholarship->scholarship_title         = $request->get('scholarship_title');
            $scholarship->link                      = $request->get('link');
            $scholarship->scholarship_filename      = NULL;
            $scholarship->scholarship_path          = NULL;
            $scholarship->scholarship_description   = $request->get('scholarship_description');

            $scholarship->save();

        }

        $res = [
            'success' => true,
            'data'    => $scholarship,
            'message' => 'Scholarship Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Scholarship  $scholarship
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scholarship = Scholarship::find($id);
        $destinationPath = 'scholarships/';
        File::delete($destinationPath.'/'.$scholarship->scholarship_filename);
        $scholarship->delete();          
        $response=[
            'success' => true,
            'message'=> "Scholarship Deleted!",
            'data' => $scholarship
        ];      
        return response($response,201);
    }
    public function download($file){                        
        $pathToFile = public_path('scholarships/'.$file);      
        $headers = array(
            'Content-Type: application/pdf',
          );

        return response()->download($pathToFile);        
        // return Response::download($pathToFile, $file, $headers);


    }
}
