<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;
use App\Mail\EventMailer;
use Validator;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $events = Event::latest()->with('user')->has('user')->get();        
        return response()->json([
            'data' => $events,            
        ]);        
        return response()->json($events);        
    }

    public function manageEvents(Request $request)
    {
 
        switch ($request->type) {
           case 'add':
              $event = Event::create([
                    'user'              => $request->user,
                    'event_title'       => $request->event_title,
                    'event_description' => $request->event_description,
                    'start'             => $request->start,
                    'end'               => $request->end,
                    'start_time'        => $request->start_time,
                    'end_time'          => $request->end_time
              ]);
              $users = User::where('email_notification',1)->get();
              foreach($users as $user){
                $details = [
                        'greeting' => 'Hi',
                        'body' => 'This is our test notification',
                        'thanks' => 'Thanks!',
                ];
                $user->notify(new \App\Notifications\EventNotification($details));
              }
            
              return response()->json($event);
             break;
  
           case 'update':
              $event = Event::find($request->id)->update([
                'user'              => $request->user,
                'event_title'       => $request->event_title,
                'event_description' => $request->event_description,
                'start'             => $request->start,
                'end'               => $request->end,
                'start_time'        => $request->start_time,
                'end_time'          => $request->end_time
              ]);
 
              return response()->json($event);
             break;
  
           case 'delete':
              $event = Event::find($request->id)->delete();
  
              return response()->json($event);
             break;
             
           default:
             # code...
             break;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [            
            // 'end_date'              => 'required',
            // 'end_time'              => 'required',
            // 'event_description'     => 'required',            
            // 'event_title'           => 'required',
            // 'start_date'            => 'required',
            // 'start_time'            => 'required'
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }                                
        $event = Event::create($request->all());   

        $res = [
            'success' => true,
            'data'    => $event,
            'message' => 'Event Uploaded'
        ];
        return response()->json($res, 201);     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $event = Event::where('id',$id)->with('user')->has('user')->first();
        // $pto = Pto::with('user')->first($id);
        return response()->json([
            'data' => $event,            
        ]);        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [            
            'start_date'       => 'nullable|date',
            'end_date'         => 'nullable|date',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);            
        }

        $event = Event::find($id);
        $event->update($request->all());

        $res = [
            'success' => true,
            'data'    => $event,
            'message' => 'Pto Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
