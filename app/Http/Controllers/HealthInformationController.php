<?php

namespace App\Http\Controllers;

use App\Models\HealthInformation;
use App\Models\User;
use App\Models\TabDescription;
use Illuminate\Http\Request;
use Validator;
use Auth;
use File;
class HealthInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');        

        if($key){         
            $health_informations = HealthInformation::where('title','LIKE',"%{$key}%")->with('user')->has('user')->latest()->paginate(5);
        }
        else{
            $health_informations = HealthInformation::with('user')->has('user')->latest()->paginate(5);
        }

        $tab_description = TabDescription::where('tab_name','=','health_informations')->first();
        return response()->json([
            'data' => $health_informations,
            'tab_description' => $tab_description,
        ]);     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $data = $request->all();
        $validator = Validator::make($data, [            
            'title'        => 'required|string',                        
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Errors']);
            
        }
        $file_name = '';
        $file_path = '';

        if($request['file']){     
            $health_information = $request->file('file');            
            $file_name = time().'.'.$health_information->extension(); //Custom Filename

            $health_information->move(public_path('health_informations'), $file_name);
            $destinationPath = 'health_informations/';
            $file_path = $destinationPath.$file_name;                                  
        }      
        $health_information = HealthInformation::create([
                
            'title'        =>$request['title'],
            'link'         =>$request['link'],
            'filename'     =>$file_name ? $file_name : NULL,
            'path'         =>$file_path ? $file_path : NULL,
            'description'  =>$request['description'],
            'uploaded_by'  =>$request->user()->id,
        ]);            


        $res = [
            'success' => true,
            'data'    => $health_information,
            'message' => 'HealthInformation Uploaded'
        ];
        return response()->json($res, 201);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HealthInformation  $healthInformation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $health_information = HealthInformation::find($id);
        return response()->json($health_information);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HealthInformation  $healthInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(HealthInformation $healthInformation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HealthInformation  $healthInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title'        => 'required|string'            
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $file_name = "";
        $file_path = "";

        if($request['file']){                       
            $health_information = $request->file('file');
            $file_name = time().'.'.$health_information->extension(); //Custom Filename
            $health_information->move(public_path('health_informations'), $file_name);
            $destinationPath = 'health_informations/';
            $file_path = $destinationPath.$file_name;   
            $health_information = HealthInformation::find($id);    

            if($health_information->filename){
                File::delete($destinationPath.'/'.$health_information->filename);        
            }
                                                
        }
        
        $health_information = HealthInformation::find($id);    
        $health_information->link          = $request->get('link') != 'null' ? $request->get('link'): Null;     
        $health_information->title         = $request->get('title');
        $health_information->filename      = $file_name ? $file_name : NULL;
        $health_information->path          = $file_path ? $file_path : NULL;
        $health_information->description   = $request->get('description') != 'null' ? $request->get('description'): Null; 
        $health_information->save();
     
        $res = [
            'success' => true,
            'data'    => $health_information,
            'message' => 'HealthInformation Updated!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HealthInformation  $healthInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $health_information = HealthInformation::find($id);
        $destinationPath = 'health_informations/';
        File::delete($destinationPath.'/'.$health_information->filename);
        $health_information->delete();          
        $response=[
            'success' => true,
            'message'=> "HealthInformation Deleted!",
            'data' => $health_information
        ];      
        return response($response,201);
    }
}
