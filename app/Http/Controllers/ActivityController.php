<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\User;
use App\Models\UserClick;
use App\Models\Thread;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DB;
class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $activities = $request->user()->unreadNotifications;            
        $response=[
            'success' => true,
            'data' => $activities,
        ];  
        return response($response,201);

    }

    public function monthly_active_users(Request $request)
    { 
        
    // Get the current year
    $currentYear = Carbon::now()->year;

    // Initialize an array to store the monthly data
    $monthlyActiveUsersByLocation = [];

    // Loop through each month of the year
    for ($month = 1; $month <= 12; $month++) {
        // Get the start and end date of the month
        $startDate = Carbon::createFromDate($currentYear, $month, 1)->startOfMonth();
        $endDate = $startDate->copy()->endOfMonth();

        // Retrieve user data for the current month, grouped by location
        $monthlyUsers = User::whereBetween('last_login_at', [$startDate, $endDate])
            ->select('location', DB::raw('COUNT(*) as totalLogin'))
            ->groupBy('location')
            ->get();

        // Map the data to the desired format
        $locationData = [];
        $totalLogins = 0;

        foreach ($monthlyUsers as $user) {
            $locationData[$user->location] = $user->totalLogin;
            $totalLogins += $user->totalLogin;
        }

        // Add the total count for all locations
        $locationData['All'] = $totalLogins;

        // Store the monthly data
        $monthlyActiveUsersByLocation[] = array_merge(['month' => $startDate->format('F')], $locationData);
    }

    // Format response
    $response = [
        'success' => true,
        'monthly_active_users_by_location' => $monthlyActiveUsersByLocation,
    ];

    return response()->json($response, 200);

 

    }

    public function monthly_active_usertypes(Request $request)
    { 
        $currentYear = Carbon::now()->year;

        // Initialize an array to store the monthly data
        $monthlyData = [];

        // Loop through each month of the year
        for ($month = 1; $month <= 12; $month++) {
            // Get the month's name
            $monthName = Carbon::create($currentYear, $month, 1)->format('F');

            // Retrieve user data for the current month, grouped by location and user type
            $userData = User::whereYear('last_login_at', $currentYear)
                ->whereMonth('last_login_at', $month)
                ->where('location', '!=' ,"All")
                ->select('location', 'user_type', DB::raw('COUNT(*) as totalLogin'))
                ->groupBy('location', 'user_type')
                ->get();

            // Initialize an array for the current month's data
            $monthData = [
                'month' => $monthName,
                'location' => [],
            ];

            // Process each user type's data for each location
            foreach ($userData as $user) {
                // Check if the location exists in the month's data array
                if (!isset($monthData['location'][$user->location])) {
                    $monthData['location'][$user->location] = [];
                }

                // Add the user type and its count to the location's data
                $monthData['location'][$user->location][$user->user_type] = $user->totalLogin;
            }

            // Add the month's data to the monthly data array
            $monthlyData[] = $monthData;
        }

        // Format the response
        $response = [
            'success' => true,
            'monthlyData' => $monthlyData,
        ];

        return response()->json($response, 200);

    }
    
    public function monthly_richmond_views(Request $request)
    { 
        // Get the current year
        $currentYear = Carbon::now()->year;

        // Initialize an array to store monthly Richmond views for each month
        $monthlyRichmondViewsByMonth = [];

        // Loop through each month of the year
        for ($month = 1; $month <= 12; $month++) {
            // Get the start and end date of the month
            $startDate = Carbon::createFromDate($currentYear, $month, 1)->startOfMonth();
            $endDate = $startDate->copy()->endOfMonth();

            // Retrieve Richmond threads for the current month with eager loading of related data
            $richmondThreads = Thread::where('location', 'Richmond')
                ->with('creator', 'replies', 'reactions')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->get();

            // Calculate total Richmond threads, reactions, and replies for the current month
            $totalRichmondThreads = $richmondThreads->count();
            $totalReactions = $richmondThreads->sum(function ($thread) {
                return $thread->reactions->count();
            });
            $totalReplies = $richmondThreads->sum(function ($thread) {
                return $thread->replies->count();
            });

            // Store total Richmond threads, reactions, and replies for the current month
            $monthlyRichmondViewsByMonth[] = [
                'month' => $startDate->format('F'),
                'TotalRichmondThreads' => $totalRichmondThreads,
                'TotalReactions' => $totalReactions,
                'TotalReplies' => $totalReplies,
            ];
        }

        // Prepare the response
        $response = [
            'success' => true,
            'monthlyRichmondViews' => $monthlyRichmondViewsByMonth,
        ];

        // Return the response
        return response()->json($response, 201);           

    }


    public function monthly_hamburg_views(Request $request)
    { 

        // Get the current year
        $currentYear = Carbon::now()->year;

        // Initialize an array to store monthly Hamburg views for each month
        $monthlyHamburgViewsByMonth = [];

        // Loop through each month of the year
        for ($month = 1; $month <= 12; $month++) {
            // Get the start and end date of the month
            $startDate = Carbon::createFromDate($currentYear, $month, 1)->startOfMonth();
            $endDate = $startDate->copy()->endOfMonth();

            // Retrieve Hamburg threads for the current month with eager loading of related data
            $HamburgThreads = Thread::where('location', 'Hamburg')
                ->with('creator', 'replies', 'reactions')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->get();

            // Calculate total Hamburg threads, reactions, and replies for the current month
            $totalHamburgThreads = $HamburgThreads->count();
            $totalReactions = $HamburgThreads->sum(function ($thread) {
                return $thread->reactions->count();
            });
            $totalReplies = $HamburgThreads->sum(function ($thread) {
                return $thread->replies->count();
            });

            // Store total Hamburg threads, reactions, and replies for the current month
            $monthlyHamburgViewsByMonth[] = [
                'month' => $startDate->format('F'),
                'TotalHamburgThreads' => $totalHamburgThreads,
                'TotalReactions' => $totalReactions,
                'TotalReplies' => $totalReplies,
            ];
        }

        // Prepare the response
        $response = [
            'success' => true,
            'monthlyHamburgViews' => $monthlyHamburgViewsByMonth,
        ];

        // Return the response
        return response()->json($response, 201);    

    }

    public function getClickUserAverage()
    {
        $clicksByUserType = UserClick::select('user_id', DB::raw('COUNT(*) as click_count'))
            ->groupBy('user_id')
            ->with('user')
            ->get();
    
        $response=[
            'success' => true,
            'data' => $clicksByUserType,
        ];  
        return response($response,201);   

    }

    
    public function mark_read(Request $request, $id)
    { 

        $notification = $request->user()->notifications()->where('id', $id)->first();

        if ($notification) {
            $notification->markAsRead();                        
        }
        $response=[
            'success' => true,
            'id' => $id,
        ];  
        return response($response,201);                

    }

    
    public function mark_all_read(Request $request)
    {

        $user = User::find($request->user()->id);

        // $user->unreadNotifications()->update(['read_at' => now()]);   
        $user->notifications()->delete();
        $response=[
            'success' => true,
        ];          
        return response()->json([
            'data' => $response
        ]);  

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        //
    }
}
