<?php

namespace App\Http\Controllers;

use App\Models\TrainingForm;
use App\Models\Activity;
use App\Models\User;
use App\Models\TabDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Validator;
use Auth;
class TrainingFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $key = \Request::get('q');

        if($key){            
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){            
                $training_forms = TrainingForm::where('firstname','LIKE',"%{$key}%")->with('user')->has('user')->latest()->paginate(5);
            }
            else{
                $training_forms = TrainingForm::where('firstname','LIKE',"%{$key}%")->where('user_id',$request->user()->id)->with('user')->has('user')->latest()->paginate(5);
            }
        }
        else{
            if(in_array($request->user()->user_type , array('Admin','Facility Manager'))){            
                $training_forms = TrainingForm::with('user')->has('user')->latest()->paginate(5);
            }
            else{
                $training_forms = TrainingForm::where('user_id',$request->user()->id)->with('user')->has('user')->latest()->paginate(5);
            }
            
        }

        $tab_description = TabDescription::where('tab_name','=','training_forms')->first();
        return response()->json([
            'data' => $training_forms,
            'tab_description' => $tab_description,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'date_request' => 'required'            
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $training_form = TrainingForm::create([            
            'user_id'         =>$request->user()->id,            
            'firstname'       =>$request->user()->firstname,
            'lastname'        =>$request->user()->lastname,
            'position'        =>$request['position'],
            'date_request'    =>$request['date_request'],            
            'comments'        =>$request['comments'],            
        ]);        
            
        $details = [
                'greeting'           => 'Hi',                
                'thanks'             => 'Thanks!',
                'firstname'          => $request->user()->firstname,
                'lastname'           => $request->user()->lastname,
                'training_form_id'   => $training_form->id,
                'created_at'         => $training_form->created_at,
                'date_request'       => $request['date_request'],
                'comments'           => $request['comments']
        ];

        $admins = Users::whereIn('user_type',['Director','Captain'])->where('location',$request->user()->location)->get();

        foreach ($admins as $key => $admin) {
            $admin->notify(new \App\Notifications\TrainingFormNotification($details));
        }

        Notification::route('mail', 'training.coordinator@cfaeastlex.net')->notify(new \App\Notifications\TrainingFormNotification($details));
        
                
        $res = [
            'success' => true,
            'data'    => $training_form,
            'message' => 'Your Form has been published!'
        ];
        return response()->json($res, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TrainingForm  $trainingForm
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $training_form = TrainingForm::find($id);
        return response()->json($training_form);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TrainingForm  $trainingForm
     * @return \Illuminate\Http\Response
     */
    public function edit(TrainingForm $trainingForm)
    {
        //
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TrainingForm  $trainingForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'date_request'  => 'required',                        
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors(), 'Validation Error']);
            
        }
        $training_form = TrainingForm::find($id);
        $training_form->position        = $request->get('position');
        $training_form->date_request    = $request->get('date_request');  
        $training_form->comments        = $request->get('comments');        
        $training_form->save();

        $res = [
            'success' => true,
            'data'    => $training_form,
            'message' => 'Training Form Updated!'
        ];
        return response()->json($res, 201);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TrainingForm  $trainingForm
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training_form = TrainingForm::find($id);                                
        $training_form->delete();   

        $response=[
            'success' => true,
            'message'=> "Form Deleted!",
            'data' => $training_form
        ];      
        return response($response,201);
    }
}
