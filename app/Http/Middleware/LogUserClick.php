<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\UserClick;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
class LogUserClick
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (Auth::check() && $request->fullUrl() !== 'api/notif_activities') {
            $userId = Auth::id();
            
            // Retrieve the timestamp of the last click for the user
            $lastClick = UserClick::where('user_id', $userId)
                ->latest()
                ->value('created_at');

            if ($lastClick) {
                // Define the time frame (e.g., 1 minute)
                $timeFrame = Carbon::parse($lastClick)->addMinutes(1);

                // Check if the current time is within the time frame of the last click
                if (Carbon::now()->lt($timeFrame)) {
                    return $response; // Skip logging the click
                }
            }

            // Log user click if it's not already logged and not skipped
            if (!$request->attributes->has('logged_user_click')) {
                UserClick::create([
                    'user_id' => $userId,
                    'clicked_item' => $request->fullUrl(),
                    'click_type' => 'page_visit',
                ]);

                // Mark this request as having logged the user click
                $request->attributes->set('logged_user_click', true);
            }
        }

        return $response;
    }
}
