<?php

namespace App\Jobs;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use Twilio\Rest\Client;
class TestSendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $details;
    protected $user;
    public function __construct($details,$user)
    {
        $this->details = $details;
        $this->user = $user;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_TOKEN");
        $twilio_number = getenv("TWILIO_FROM");
        $client = new Client($account_sid, $auth_token);

        
        $this->user->notify(new \App\Notifications\NewsfeedNotification($this->details));
            
        if($this->details['user_type'] == 'Admin' && $this->details['send_sms'] == '0' ){
                        
            $message = $this->details['body'] . "\n";
            $message .= 'View Post'.' '.getenv("MY_APP_URL").'/'.$this->details['location']; 
            
            try {
                $number = $client->lookups->v1->phoneNumbers($this->user['phone'])->fetch(["type" => ["carrier"]]);

                $account_sid = getenv("TWILIO_SID");
                $auth_token = getenv("TWILIO_TOKEN");
                $twilio_number = getenv("TWILIO_FROM");
    
                $client = new Client($account_sid, $auth_token);
                $client->messages->create($this->user['phone'], [
                'from' => $twilio_number, 
                'body' => $message]);     
                
            } catch (\Throwable $e) {
                
            }          
        }        
    }
}
