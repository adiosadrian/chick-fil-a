<?php

namespace App\Jobs;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use Twilio\Rest\Client;
use App\Events\LikeEvent;
use Exception;
class SendNewsfeedNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 0;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $details;
    protected $user;
    public function __construct($details,$user)
    {
        $this->details = $details;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        
        


        foreach ($this->user as $key => $user) {
            $user->notify(new \App\Notifications\NewsfeedNotification($this->details));
            if($this->details['send_email'] == 0 ){        
                try {
                    $user->notify(new \App\Notifications\NewsfeedEmailNotification($this->details ,$user));
                } catch (\Exception $e) {
                    
                }                            
            }                
            if($user['email'] != "crystal@ad-ios.com" && $this->details['user_type'] == 'Admin' && $this->details['send_sms'] == 0 ){
                $account_sid = getenv("TWILIO_SID");
                $auth_token = getenv("TWILIO_TOKEN");
                $twilio_number = getenv("TWILIO_FROM");
                $client = new Client($account_sid, $auth_token);            
                $message = $this->details['body'] . "\n\n";
                $message .= 'View Post'.' '.getenv("MY_APP_URL").'/'.$this->details['itemLink']; 
                $message .= "\n\nFrom: Chick-fil-A East Lexington \n"; 
                $message .= getenv("MY_APP_URL");
                
                try {  
                    
                    $send_sms = $client->messages->create($user['phone'], [
                    'messagingServiceSid' => "MGfa62bac8f4636bc9a357d73221a6c1b8", 
                    'body' => $message]);                        
                    
                } catch (\Exception $e) {
                    
                }          
            }             
        }                                              
        broadcast(new LikeEvent());  
    }
}
