<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use App\Models\User;
use Exception;
class PostReactNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $timeout = 0;	
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $details;
    protected $user;
    public function __construct($details,$user)
    {
        $this->details = $details;
        $this->user = $user;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->user as $key => $user) {
            
            try {
                $user->notify(new \App\Notifications\PostReactNotification($this->details));                        
            } catch (\Exception $e) {
                    
            }          
            
        }
    }
}
