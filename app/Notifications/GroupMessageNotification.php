<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GroupMessageNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $files = '';
        if($this->details['file']){
            $files = explode(',',$this->details['file']);
        }        

        $path = public_path('group_messages');

        $mail =  (new MailMessage)
        ->line(__('New Message from Admin'))
        ->action('Notification Action', $this->seturl($notifiable))
        ->line(__('Thank you for using our application!'))        
        ->view('emails.group_message',['details' => $this->details,'url' => $this->seturl($notifiable)]);

        if($files){
            foreach($files as $file){            
                $mail->attach($path.'/'.$file, [                    
                ]);
    
            }
        }
        
        return $mail;

    }
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    public function toDatabase($notifiable)
    {
        return [
            'data'=>$this->details
           ];                  
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    protected function seturl($notifiable)
    {
        // $appUrl = config('app.client_url', config('app.url'));
        $appUrl = env("MY_APP_URL");
        $group_message = $this->details['group_message_id'];
        return url("$appUrl/group_message_thread?id=$group_message");
    }
}
