<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CovidFormNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->line(__('User submitted covid questionnaire'))
        ->action('Notification Action', $this->seturl($notifiable))
        ->line(__('Thank you for using our application!'))
        ->view('emails.covid_form',['details' => $this->details,'url' => $this->seturl($notifiable)]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    protected function seturl($notifiable)
    {
        // $appUrl = config('app.client_url', config('app.url'));
        $appUrl = env("MY_APP_URL");        
        return url("$appUrl/covid_questionnaire");
    }
}
