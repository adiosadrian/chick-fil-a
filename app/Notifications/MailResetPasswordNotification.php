<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Illuminate\Auth\Notifications\ResetPassword;


class MailResetPasswordNotification extends Notification
{
    use Queueable;
    
    public $token;
    public function __construct($token)
    {
        $this->token = $token;
    }
    /**
     * Create a new notification instance.
     *
     * @return void
     */


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line(__('You are receiving this email because we received a password reset request for your account.'))
            ->action(__('Reset Password'), $this->resetUrl($notifiable))
            ->line(__('If you did not request a password reset, no further action is required.'))
            ->view('emails.reset', ['url' => $this->resetUrl($notifiable)]);
    }
    

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    protected function resetUrl($notifiable)
    {
        // $appUrl = config('app.client_url', config('app.url'));
        $appUrl = env("MY_APP_URL");

        return url("$appUrl/password-reset?token=$this->token").'&email='.urlencode($notifiable->email);
    }
}
