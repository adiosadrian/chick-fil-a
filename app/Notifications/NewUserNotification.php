<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewUserNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $token;
    public function __construct($details,$token)
    {
        $this->details = $details;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->line(__('Account Details'))
        ->action(__('Set Password'), $this->setpassword($notifiable))
        ->line(__('Thank you for using our application!'))
        ->view('emails.new_user',['details' => $this->details, 'url' => $this->setpassword($notifiable)]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    protected function setpassword($notifiable)
    {
        // $appUrl = config('app.client_url', config('app.url'));
        $appUrl = env("MY_APP_URL");

        return url("$appUrl/set-password?token=$this->token").'&email='.urlencode($notifiable->email);
    }
}
