<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FacilityNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }
    

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $files = '';
        if($this->details['file']){
            $files = explode(',',$this->details['file']);
        }      
        $path = public_path('facilities');

        $mail =  (new MailMessage)
        ->line(__('New Facility Ticket'))
        ->action('Notification Action', $this->seturl($notifiable))
        ->line(__('Thank you for using our application!'))        
        ->view('emails.facility',['details' => $this->details,'url' => $this->seturl($notifiable)]);

        if($files){
            foreach($files as $file){            
                $mail->attach($path.'/'.$file, [                    
                ]);
    
            }
        }
        
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    protected function seturl($notifiable)
    {
        // $appUrl = config('app.client_url', config('app.url'));
        $appUrl = env("MY_APP_URL");
        $facility_id = $this->details['facility_id'];
        return url("$appUrl/facility_thread?id=$facility_id");
    }
}
