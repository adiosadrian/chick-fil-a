<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Carbon\Carbon;
class RemoveUserSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'removesession:removeusersession';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user_sessions = User::where('last_login_at', '<', Carbon::now()->subHours(1)->toDateTimeString())->where('isLoggedIn',0)->get();

        foreach ($user_sessions as $key => $user) {
            User::find($user->id)->update(['isLoggedIn'=> '-1']);
        }
    }
}
