<?php

namespace App\Console\Commands;
use App\Models\User;
use App\Models\Activity;
use App\Models\GroupMessage;
use Illuminate\Console\Command;
use Twilio\Rest\Client;
use App\Events\LikeEvent;
class SendGroupMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $details;
    protected $signature = 'sendmessage:sendgroupmessages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {        
        $group_messages = GroupMessage::where('is_sent', 0)->with('creator')->has('creator')->get();
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_TOKEN");
        $twilio_number = getenv("TWILIO_FROM");
        $client = new Client($account_sid, $auth_token);
        

        foreach($group_messages as $group_message){
            $user = User::find($group_message->creator->id);
            if($group_message->send_email == 0){

                $details = [
                    'greeting'          => 'Hi',                
                    'thanks'            => 'Thanks!',
                    'firstname'         => $group_message->creator['firstname'],
                    'file'              => $group_message['file'],
                    'lastname'          => $group_message->creator['lastname'],
                    'group_message_id'  => $group_message['id'],
                    'created_at'        => $group_message['created_at'],
                    'itemLink'          =>'group_message',
                    'itemPosted'        =>'New message',
                    'message'           => $group_message['message'],
                    'title'             => $group_message['title'],
                ];        
                try {
                $user->notify(new \App\Notifications\GroupMessageNotification($details));
                }
                catch(\Exception $e){

                }
                
            }
 
            if($group_message->send_sms == 0){
                                       
                
                $message = "Hi $user->firstname, \n";   
                // $message .= strip_tags($group_message['message']);  
                $message .= "New Message from Chick-fil-A \n"; 
                $message .= "View Message \n"; 
                $message .= getenv("MY_APP_URL").'/group_message_thread?id='.$group_message['id'];                 
                try {
                    // $number = $client->lookups->v1->phoneNumbers($user->phone)->fetch(["type" => ["carrier"]]);

                    $account_sid = getenv("TWILIO_SID");
                    $auth_token = getenv("TWILIO_TOKEN");
                    $twilio_number = getenv("TWILIO_FROM");
        
                    $client = new Client($account_sid, $auth_token);
                    $client->messages->create($user->phone, [
                    'messagingServiceSid' => "MGfa62bac8f4636bc9a357d73221a6c1b8", 
                    'body' => $message]);     
                    
                } catch (\Exception $e) {
                    
                }                              
            } 
                 
            $group_message = GroupMessage::find($group_message['id']);
            $group_message->is_sent     = "1";   
            $group_message->send_email  = $group_message->send_email == 0 ? 1 : 1;   
            $group_message->send_sms    = $group_message->send_sms == 0 ? 1 : 1;        
            $group_message->save();  
        }       
        broadcast(new LikeEvent());         
    }
}
