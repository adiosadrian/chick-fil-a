<?php

namespace App\Console\Commands;
use App\Models\User;
use App\Models\Thread;
use App\Models\Activity;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Events\LikeEvent;
use Exception;
class NewsfeedSendEmailUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $details;
    protected $signature = 'sendemail:sendnewsfeedemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Emails to Users when There Newsfeed is added';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //send email
        $threads = Thread::where('is_sent', 0)->with('creator')->has('creator')->get();
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_TOKEN");
        $twilio_number = getenv("TWILIO_FROM");
        $client = new Client($account_sid, $auth_token);

        
        $users = User::where('email', '=','adrian@ad-ios.com')->get();

        
        foreach($users as $user){
            
            $message = "Hi $user->firstname, \n";   
            $message .= "\nadded a new post \n";
            $message .= "\nView Post\n"; 

            $message .= "\nFrom: Chick-fil-A East Lexington \n"; 
            $message .= getenv("MY_APP_URL");


            
            try {
                $number = $client->lookups->v1->phoneNumbers($user->phone)->fetch(["type" => ["carrier"]]);
                
                $account_sid = getenv("TWILIO_SID");
                $auth_token = getenv("TWILIO_TOKEN");
                $twilio_number = getenv("TWILIO_FROM");
    
                $client = new Client($account_sid, $auth_token);
                $test = $client->messages->create($user->phone, [
                'messagingServiceSid' => "MGfa62bac8f4636bc9a357d73221a6c1b8", 
                'body' => $message]);     

                print($test->sid);
                
            } catch (\Exception $e) {
                
            }          


        }


        // foreach($threads as $thread){
        //     if($thread->send_email == 0){

        //         foreach($users as $user){
        //             $details = [
        //                     'greeting'      => 'Hi',
        //                     'body'          => $thread->creator['firstname'] . ' added a new post',
        //                     'thanks'        => 'Thanks!',
        //                     'firstname'     => $user->firstname,
        //                     'thread_id'     => $thread['thread_id'],
        //                     'itemPosted'    =>$thread->creator['firstname']  . ' added a new post',
        //                     'itemLink'      =>$thread['location'] == "All" ? 'news' : $thread['location'],
        //                     'location'      => $thread['location'],
        //                     'created_at'    => $thread['created_at'],                            
        //                     'title'         => $thread['title'],
        //                     'url'           => $thread['location'] == "All" ? 'news' : $thread['location'],
        //             ];    
        //             \Mail::to($user->email)->send(new \App\Mail\NewsfeedNotification($details));
        //         }    
        //     }
        //     if($thread->send_sms == 0){
                                       
        //         foreach($users as $user){
        //             $message = "Hi $user->firstname, \n";   
        //             $message .= $thread->creator['firstname'].' '."added a new post \n";
        //             $message .= 'View Post'.' '.getenv("MY_APP_URL").'/'.$thread->location; 
                    
        //             try {
        //                 $number = $client->lookups->v1->phoneNumbers($user->phone)->fetch(["type" => ["carrier"]]);
    
        //                 $account_sid = getenv("TWILIO_SID");
        //                 $auth_token = getenv("TWILIO_TOKEN");
        //                 $twilio_number = getenv("TWILIO_FROM");
            
        //                 $client = new Client($account_sid, $auth_token);
        //                 $client->messages->create($user->phone, [
        //                 'from' => $twilio_number, 
        //                 'body' => $message]);     
                        
        //             } catch (\Throwable $e) {
                        
        //             }          


        //         }
        //     }              
        //     $thread = Thread::find($thread['id']);
        //     $thread->is_sent     = "1";   
        //     $thread->send_email  = $thread->send_email == 0 ? 1 : 1;   
        //     $thread->send_sms    = $thread->send_sms == 0 ? 1: 1;          
        //     $thread->save();  
        // }    
        
        
    }
}
