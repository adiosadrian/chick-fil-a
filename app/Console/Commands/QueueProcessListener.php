<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class QueueProcessListener extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $command = 'php artisan queue:listen --daemon';
    protected $signature = 'queue-process-listener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $arguments = '--tries=3';
    
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->isProcessRunning($this->command)) {
            $this->info("Starting queue listener.");
            $this->executeShellCommand($this->command, $this->arguments, true);
        } else {
            $this->info("Queue listener is running.");
        }
    }
    public function executeShellCommand($command, $arguments = '', $background = false)
    {
        $command = trim($command);
        if (!is_string($command) || empty($command)) {
            return null;
        }

        $arguments = trim($arguments);

        $cmd = trim($command . ' ' . $arguments) . ($background ? ' > /dev/null 2>/dev/null &' : '');
        return shell_exec($cmd);
    }
    public function isProcessRunning($process)
    {
        $output = $this->executeShellCommand('pgrep -f "' . $process . '"');

        return !empty(trim($output));
    }
}
