<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\NewsfeedSendEmailUser',
        'App\Console\Commands\SendGroupMessage',
        'App\Console\Commands\RemoveUserSession',
        'App\Console\Commands\QueueProcessListener',
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        // $schedule-> command('sendemail:sendnewsfeedemail')->everyMinute();
        $schedule->command('sendmessage:sendgroupmessages')->everyMinute();
        $schedule->command('removesession:removeusersession')->everyMinute();
        // $schedule->command('queue:restart')->everyFiveMinutes();
        // $schedule->command('queue:retry all')->everyMinute();
	    $schedule->command('queue:work --sleep=5 --timeout=0')->everyMinute()->withoutOverlapping();        
        // $schedule->command('queue-process-listener')->everyMinute()->withoutOverlapping();
        // Artisan::call('queue:work');


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
