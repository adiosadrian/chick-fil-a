<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Appreciation extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [        
        'appreciation_title',
        'appreciation_filename',
        'appreciation_path',
        'appreciation_description',        
        'appreciation_type',
        'location'
    ];
    protected $dates = [ 'deleted_at' ];
}
