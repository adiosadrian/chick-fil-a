<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;
    protected $table = "user_types";
    protected $fillable = [
        'title',
    ];

    public function permission()
    {
        return $this->hasMany(Permission::class,'role_id');
    }
}
