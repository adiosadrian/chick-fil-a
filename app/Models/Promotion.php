<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [        
        'promotion_title',
        'promotion_filename',
        'promotion_path',
        'promotion_description',        
        'promotion_type',
        'location',
    ];
    protected $dates = [ 'deleted_at' ];
}
