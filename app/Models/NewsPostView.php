<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsPostView extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'thread_id', 'location', 'viewed_at'];

    public function user() {
        return $this->belongsTo(User::class);
    }

}
