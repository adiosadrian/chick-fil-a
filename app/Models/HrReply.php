<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HrReply extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['owner'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($hr_reply){
            $hr_reply->hr_form->increment('replies_count');
        });

        static::deleted(function($hr_reply){
            $hr_reply->hr_form->decrement('replies_count');
        });
    }

    // A Reply has an owner
    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
    public function hr_form()
    {
        return $this->belongsTo(HrForm::class);
    }

}
