<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Form extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'form_title',
        'link',
        'form_filename',
        'form_path',
        'form_description',
        'uploaded_by'
    ];
    protected $dates = [ 'deleted_at' ];

    public function user()
    {
        return $this->belongsTo(User::class, 'uploaded_by', 'id');
    }
}
