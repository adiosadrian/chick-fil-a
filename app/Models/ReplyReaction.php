<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReplyReaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'reaction',
        'reply_id'

    ];
    protected $with = ['owner'];
    // A Reaction has an owner
    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function reply()
    {
        return $this->belongsTo(Reply::class);
    }
}
