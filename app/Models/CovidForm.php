<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CovidForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'location',
        'name',
        'is_vaccinated',
        'filename',
        'full_path',
    ];    

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
