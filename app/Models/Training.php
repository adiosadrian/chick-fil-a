<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Training extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'category_id',
        'training_title',
        'training_filename',
        'training_path',
        'user_type',
        'location',
        'link',
        'training_description',
        'uploaded_by'
    ];

    protected $dates = [ 'deleted_at' ];

    public function user()
    {
        return $this->belongsTo(User::class, 'uploaded_by', 'id');
    }
}
