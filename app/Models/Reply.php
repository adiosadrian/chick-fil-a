<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\Favoritable;

class Reply extends Model
{
    use HasFactory;
    use Favoritable;

    protected $guarded = [];

    // eager load relationship for every query
    protected $with = ['owner', 'favorites','reactions'];

    // append custom attribute (getFavoritesCountAttribute) from Trait
    protected $appends = ['favoritesCount', 'isFavorited'];

	// model event for incrementing thread's replies count
    protected static function boot()
    {
        parent::boot();

        static::created(function($reply){
            $reply->thread->increment('replies_count');
        });

        static::deleted(function($reply){
            $reply->thread->decrement('replies_count');
            $reply->reactions->each->delete();
        });
    }

    // A Reply has an owner
    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }
    public function reactions()
    {
    	return $this->hasMany(ReplyReaction::class);
    }

    public function path()
    {
        return $this->thread->path() . "#reply-{$this->id}";
    }

}
