<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupMessageReply extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['owner'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($group_message_reply){
            $group_message_reply->group_message->increment('replies_count');
        });

        static::deleted(function($group_message_reply){
            $group_message_reply->group_message->decrement('replies_count');
        });
    }

    // A Reply has an owner
    public function owner()
    {
    	return $this->belongsTo(User::class, 'from');
    }
    public function group_message()
    {
        return $this->belongsTo(GroupMessage::class);
    }
}
