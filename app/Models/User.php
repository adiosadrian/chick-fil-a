<?php

namespace App\Models;

use App\Notifications\MailResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Cache;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'firstname',        
        'lastname',        
        'email',
        'mobile_number',
        'phone',
        'date_of_birth',
        'date_hired',
        'address',
        'city',
        'state',
        'location',
        'zip',
        'profile_filename',
        'profile_path',
        'password',
        'user_type',   
        'facebook_link',   
        'instagram_link',   
        'google_link',   
        'last_login_at',
        'last_login_ip',     
        'isLoggedIn'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function threads()
    {
        // make database relationship and return threads in proper order
        return $this->hasMany(Thread::class)->latest();
    }

    public function reactions()
    {
        // make database relationship and return threads in proper order
        return $this->hasMany(ThreadReaction::class)->latest();
    }

    // Get route key name for Laravel
    public function getRouteKeyName()
    {
        return 'name';
    }

    // Get all activity for the user
    public function activity()
    {
        return $this->hasMany(Activity::class);
    }

    public function active_user()
    {
        return $this->hasMany(ActiveUser::class);
    }
    

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public function sendPasswordResetNotification($token)
    {        
        $this->notify(new MailResetPasswordNotification($token));
    }
    public function getProfileFilenameAttribute($value)
    {
        // If profile_filename is null, return "profile-user.png" instead
        return $value ?? 'profile-user.png';
    }
    public function getProfilePathAttribute($value)
    {
        // If profile_filename is null, return "profile-user.png" instead
        return $value ?? 'profiles/profile-user.png';
    }
}
