<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class HrDoc extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [        
        'hr_doc_title',
        'link',
        'hr_doc_filename',
        'hr_doc_path',
        'hr_doc_description',
        'uploaded_by'
    ];
    protected $dates = [ 'deleted_at' ];

    public function user()
    {
        return $this->belongsTo(User::class, 'uploaded_by', 'id');
    }
}
