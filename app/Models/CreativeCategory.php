<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CreativeCategory extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'category_title',
        'category_description'
    ];
    protected $dates = [ 'deleted_at' ];
}
