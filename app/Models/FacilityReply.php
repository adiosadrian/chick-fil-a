<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FacilityReply extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['owner'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($reply){
            $reply->facility->increment('replies_count');
        });

        static::deleted(function($reply){
            $reply->facility->decrement('replies_count');
        });
    }

    // A Reply has an owner
    public function owner()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
    public function facility()
    {
        return $this->belongsTo(Facility::class);
    }
}
