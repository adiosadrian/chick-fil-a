<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupMessage extends Model
{
    use HasFactory;
    protected $guarded = [];

    protected $with = ['creator'];

    protected static function boot()
    {
        parent::boot();
        // delete associated replies when removing thread
        static::deleting(function($thread){        
            $thread->message_replies->each->delete();            
        });
    }


    public function creator()
    {
    	return $this->belongsTo(User::class, 'to');
    }
    public function message_replies()
    {
    	return $this->hasMany(GroupMessageReply::class);
    }
}
