<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HealthInformation extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'link',
        'filename',
        'path',
        'description',
        'uploaded_by',
    ];    

    public function user()
    {
        return $this->belongsTo(User::class, 'uploaded_by', 'id');
    }
}
