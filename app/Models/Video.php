<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Video extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'category_id',
        'video_title',
        'video_filename',
        'video_path',
        'video_description',
        'uploaded_by'
    ];
    protected $dates = [ 'deleted_at' ];
}
