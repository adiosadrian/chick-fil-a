<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    protected $with = ['creator'];
    protected static function boot()
    {
        parent::boot();        
        static::deleting(function($facilities){        
            $facilities->facility_thread->each->delete();            
        });
    }
    public function facility_thread()
    {
    	return $this->hasMany(FacilityReply::class)->orderBy('id','DESC');
    }

    public function creator()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
