<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Event extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user',
        'event_title',
        'event_description',
        'events_location',
        'start_date',
        'end_date',
        'start_time',
        'end_time'
    ];

    protected $dates = [ 'deleted_at' ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user', 'id');
    }
}
