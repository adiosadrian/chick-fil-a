<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->integer('user')->length(11)->unsigned();
            $table->string('event_title')->nullable();            
            $table->string('event_description')->nullable();
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();      
            $table->softDeletes();           
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
