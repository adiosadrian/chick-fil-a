<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');            
            $table->unsignedInteger('replies_count')->default(0);
            $table->string('title')->nullable();
            $table->string('user_type')->nullable();
            $table->string('location')->nullable();
            $table->text('message')->nullable();         
            $table->integer('is_sent');     
            $table->integer('send_email');     
            $table->integer('send_sms');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_messages');
    }
}
