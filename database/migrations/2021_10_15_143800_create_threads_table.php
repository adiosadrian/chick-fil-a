<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('threads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');        
            $table->string('location')->nullable();
            $table->unsignedInteger('replies_count')->default(0);     
            $table->text('title')->nullable();
            $table->text('body')->nullable();
            $table->integer('is_featured')->default('0');     
            $table->integer('is_sent')->default('0');     
            $table->integer('send_email')->default('0');     
            $table->integer('send_sms')->default('0');                   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('threads');
    }
}
