<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_docs', function (Blueprint $table) {
            $table->id();            
            $table->string('hr_doc_title')->nullable();
            $table->string('link')->nullable();
            $table->string('hr_doc_filename')->nullable();
            $table->string('hr_doc_path')->nullable();
            $table->string('hr_doc_description')->nullable();
            $table->string('uploaded_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_docs');
    }
}
