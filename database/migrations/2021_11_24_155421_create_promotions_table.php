<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->id();            
            $table->string('promotion_title')->nullable();
            $table->string('promotion_filename')->nullable();
            $table->string('promotion_path')->nullable();
            $table->string('promotion_description')->nullable();        
            $table->string('promotion_type')->nullable();   
            $table->string('location')->nullable();        
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
