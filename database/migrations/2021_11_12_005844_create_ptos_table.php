<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePtosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ptos', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');            
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('total_hours')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();            
            $table->string('approved')->default('pending');   
            $table->string('approved_by')->nullable();    
            $table->string('type')->nullable();                      
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ptos');
    }
}
