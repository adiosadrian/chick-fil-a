<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHrFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');            
            $table->unsignedInteger('replies_count')->default(0);
            $table->string('title')->nullable();
            $table->text('message')->nullable();
            $table->unsignedInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hr_forms');
    }
}
