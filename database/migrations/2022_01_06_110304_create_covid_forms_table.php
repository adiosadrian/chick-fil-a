<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCovidFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('covid_forms', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');            
            $table->string('location')->nullable();
            $table->string('name')->nullable();
            $table->string('is_vaccinated')->nullable();   
            $table->string('filename')->nullable();         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('covid_forms');
    }
}
