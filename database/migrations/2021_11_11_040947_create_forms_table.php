<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->string('form_title')->nullable();
            $table->string('link')->nullable();
            $table->string('form_filename')->nullable();
            $table->string('form_path')->nullable();
            $table->string('form_description')->nullable();
            $table->string('uploaded_by')->nullable();
            $table->string('user_type')->nullable();
            $table->string('location')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
