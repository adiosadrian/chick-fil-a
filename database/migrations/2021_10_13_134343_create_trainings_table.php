<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable();
            $table->string('training_title')->nullable();
            $table->string('link')->nullable();
            $table->string('training_filename')->nullable();
            $table->string('training_path')->nullable();
            $table->string('training_description')->nullable();      
            $table->string('uploaded_by')->nullable();
            $table->string('user_type')->nullable();
            $table->string('location')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
