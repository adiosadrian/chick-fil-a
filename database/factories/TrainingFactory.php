<?php

namespace Database\Factories;

use App\Models\Training;
use App\Models\TrainingCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrainingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Training::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [            
            'uploaded_by' => '1',
            'training_filename'  => '1634831348_file-sample_100kB.doc',
            'training_path'    => '/storage/uploads/trainings/1634831348_file-sample_100kB.doc',
            'training_title' => $this->faker->word,
            'training_description' => $this->faker->sentence,
        ];
    }
}