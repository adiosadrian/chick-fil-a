<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $password;

        return [
            
            'firstname'        => $this->faker->name,            
            'lastname'         => $this->faker->name,            
            'email'            => $this->faker->unique()->safeEmail,
            'mobile_number'    => $this->faker->phoneNumber,
            'phone'            => $this->faker->phoneNumber,
            'date_of_birth'    => '1997-10-06',
            'date_hired'       => '2021-10-06',
            'city'             => '1175 Peachtree St NE Suite',
            'location'         => 'Hamburg',
            'user_type'        => 'Captain',
            'state'            => 'Atlanta',
            'zip'              => 'GA 30361 ',
            'password'         => $password ?: $password = bcrypt('secret'),
            'remember_token'   => Str::random(10),

        ];
    }
}
