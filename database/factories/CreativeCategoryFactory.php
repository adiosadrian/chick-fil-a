<?php

namespace Database\Factories;

use App\Models\CreativeCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class CreativeCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CreativeCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_title' => $this->faker->unique()->word,
            'category_description' => $this->faker->sentence,
        ];
    }
}
