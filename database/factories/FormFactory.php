<?php

namespace Database\Factories;

use App\Models\Form;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Form::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => '1',
            'uploaded_by' => function(){                
                return User::factory()->create()->id;
            },
            'form_filename'  => '1634831348_file-sample_100kB.doc',
            'form_path'    => '/storage/uploads/forms/1634831348_file-sample_100kB.doc',
            'form_title' => $this->faker->word,
            'form_description' => $this->faker->sentence,
        ];
    }
}
