<?php

namespace Database\Factories;

use App\Models\Creative;
use App\Models\CreativeCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CreativeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Creative::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => function(){                
                return CreativeCategory::factory()->create()->id;
            },
            'uploaded_by' => function(){                
                return User::factory()->create()->id;
            },
            'creative_filename'  => '1634831005_download.png',
            'creative_path'    => '/storage/uploads/creatives/1634831005_download.png',
            'creative_title' => $this->faker->word,
            'creative_description' => $this->faker->sentence,
        ];
    }
}
