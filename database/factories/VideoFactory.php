<?php

namespace Database\Factories;

use App\Models\Video;
use App\Models\VideoCategory;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class VideoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Video::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => function(){                
                return VideoCategory::factory()->create()->id;
            },
            'uploaded_by' => function(){                
                return User::factory()->create()->id;
            },
            'video_filename'  => '1634830758_sample-mp4-file.mp4',
            'video_path'    => '/storage/uploads/videos/1634830758_sample-mp4-file.mp4',
            'video_title' => $this->faker->word,
            'video_description' => $this->faker->sentence,
        ];

    }
}
