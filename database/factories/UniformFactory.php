<?php

namespace Database\Factories;

use App\Models\Uniform;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UniformFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Uniform::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => "1",            
            'uploaded_by' => function(){                
                return User::factory()->create()->id;
            },
            'uniform_filename'  => '1634831005_download.png',
            'uniform_path'    => '/storage/uploads/uniforms/1634831005_download.png',
            'uniform_title' => $this->faker->word,
            'uniform_description' => $this->faker->sentence,
        ];
    }
}
