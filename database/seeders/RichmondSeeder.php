<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;
use Auth;
use File;
class RichmondSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(public_path("richmond_users.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            if (!$firstline) {

                if (!User::where('email',$data['3'])->exists()) {
                    $user= User::create([
                        "firstname" => $data['0'],
                        "lastname"  => $data['1'],
                        "phone"     => $data['2'],
                        "email"     => $data['3'],                    
                        'user_type' => $data['4'],      
                        'location'  => "Richmond",              
                        'password'  =>bcrypt("password"),
                        
                    ]);    
                    $token=$user->createToken('myapptoken')->plainTextToken;
    
                    $details = [
                        'greeting' => 'Hi',
                        'firstname' =>$data['0'],
                        'lastname'  =>$data['1'],
                        'email'     =>$data['3'],                
                        'thanks'    => 'Thanks!',
                    ];
    
                    $user->notify(new \App\Notifications\NewUserNotification($details,$token));  
                 }
                
            }
            $firstline = false;
            
        }
   
        fclose($csvFile);
    }
}
