<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Thread;
use App\Models\Reply;
use App\Models\User;
use App\Models\Creative;
use App\Models\Training;
use App\Models\Video;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        // Thread::factory()->count(10)->create();
        // Reply::factory()->count(5)->create();
        // Creative::factory()->count(5)->create();
        // Training::factory()->count(5)->create();
        // Video::factory()->count(5)->create();
        User::factory()->count(200)->create();
    }
}
