<?php

namespace Database\Seeders;
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;
use App\Models\Thread;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	// Creates threads, replies and associated channels and users
        Thread::factory()->count(10)->create();
        Reply::factory()->count(5)->create();
    }
}
