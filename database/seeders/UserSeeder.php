<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;
use Auth;
use File;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *\
     * @return void
     */
    public function run()
    {
        $users = User::whereIn('email', ["keyssdanispano@gmail.com", "refunkedjunquequeen@gmail.com","Chucho102016@icloud.com","nathan.sharrett87@gmail.com"])->get();
    
        foreach ($users as $key => $user) {

            $token=$user->createToken('myapptoken')->plainTextToken;
    
            $details = [
                'greeting' => 'Hi',
                'firstname' =>$user['firstname'],
                'lastname'  =>$user['lastname'],
                'email'     =>$user['email'],         
                'thanks'    => 'Thanks!',
            ];

            $user->notify(new \App\Notifications\NewUserNotification($details,$token));  
        }

        
    }
}
