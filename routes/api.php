<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\VideoController;
use App\Http\Controllers\PtoController;
use App\Http\Controllers\VacationController;
use App\Http\Controllers\UniformController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\CreativeController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\ThreadSubscriptionsController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\HrDocController;
use App\Http\Controllers\ScholarshipController;
use App\Http\Controllers\TabDescriptionController;
use App\Http\Controllers\AppreciationController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\HrReplyController;
use App\Http\Controllers\HrFormController;
use App\Http\Controllers\GroupMessageController;
use App\Http\Controllers\GroupMessageReplyController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\FacilityReplyController;
use App\Http\Controllers\FileManagerController;
use App\Http\Controllers\TrainingFormController;
use App\Http\Controllers\CovidFormController;
use App\Exports\CovidFormExport;
use App\Http\Controllers\HealthInformationController;




// use App\Http\Controllers\ForgotPasswordController;
// use App\Http\Controllers\ResetPasswordController;
use App\Mail\EventMailer;

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register',[AuthController::class,'register']);
Route::post('/login', [AuthController::class, 'signin']);


// Route::post('/password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
// Route::post('/password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
// Route::post('/password/reset', [ResetPasswordController::class, 'reset']);
Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
Route::post('password/reset', [ResetPasswordController::class, 'reset']);




// Route::get('/users', [AuthController::class,'index']);
// Route::get('/users/{id}', [AuthController::class,'show']);
// Route::get('/users/search/{search}', [AuthController::class,'search_unit_by_key']);

Route::get('/users', [AuthController::class,'index']);
Route::get('/users/{id}', [AuthController::class,'show']);
Route::get('/search_user', [AuthController::class,'search_user_by_key']);


//Videos
Route::prefix('videos')->group(function () {    
    Route::get('/', [VideoController::class,'index']);
    Route::get('/{id}', [VideoController::class,'show']);
    Route::post('/',[VideoController::class,'store']);
    Route::put('/{id}',[VideoController::class,'update']);
    Route::delete('/{id}',[VideoController::class,'destroy']);
    Route::get('/download/{file}',[VideoController::class,'download']);    
});
Route::get('/search_video', [VideoController::class,'search_video_by_key']);


//Appreciation
Route::group(['prefix' => 'appreciations', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [AppreciationController::class,'index']);
    Route::get('/{id}', [AppreciationController::class,'show']);
    Route::post('/',[AppreciationController::class,'store']);
    Route::put('/{id}',[AppreciationController::class,'update']);
    Route::delete('/{id}',[AppreciationController::class,'destroy']);
    Route::get('/download/{file}',[AppreciationController::class,'download']);    
});
Route::get('/search_appreciation', [AppreciationController::class,'search_appreciation_by_key']);


//Promotions
Route::group(['prefix' => 'promotions', 'middleware' => 'auth:sanctum'], function() {      
    Route::get('/', [PromotionController::class,'index']);
    Route::get('/{id}', [PromotionController::class,'show']);
    Route::post('/',[PromotionController::class,'store']);
    Route::put('/{id}',[PromotionController::class,'update']);
    Route::delete('/{id}',[PromotionController::class,'destroy']);
    Route::get('/download/{file}',[PromotionController::class,'download']);    
});
Route::get('/search_promotion', [PromotionController::class,'search_promotion_by_key']);

//PTO
Route::group(['prefix' => 'ptos', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [PtoController::class,'index']);
    Route::get('/{id}', [PtoController::class,'employee_pto']);
    Route::get('/{id}', [PtoController::class,'show']);    
    Route::post('/',[PtoController::class,'store']);
    Route::put('/{id}',[PtoController::class,'update']);
    Route::delete('/{id}',[PtoController::class,'destroy']);    
    
});

//Vacation
Route::group(['prefix' => 'vacations', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [VacationController::class,'index']);    
});


//  Trainings
Route::group(['prefix' => 'trainings', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [TrainingController::class,'index']);
    Route::get('/latest', [TrainingController::class,'latest']);
    Route::get('/{id}', [TrainingController::class,'show']);
    Route::post('/',[TrainingController::class,'store']);
    Route::put('/{id}',[TrainingController::class,'update']);
    Route::delete('/{id}',[TrainingController::class,'destroy']);
    Route::GET('/download/{file}',[TrainingController::class,'download']);
});
Route::get('/search_training', [TrainingController::class,'search_training_by_key']);

//  Covid Forms
Route::group(['prefix' => 'health_informations', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [HealthInformationController::class,'index']);    
    Route::get('/{id}', [HealthInformationController::class,'show']);
    Route::post('/',[HealthInformationController::class,'store']);
    Route::put('/{id}',[HealthInformationController::class,'update']);
    Route::delete('/{id}',[HealthInformationController::class,'destroy']);
    Route::get('/download/{file}',[HealthInformationController::class,'download']);    
});

Route::group(['prefix' => 'covid_forms', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [CovidFormController::class,'index']);
    Route::get('/latest', [CovidFormController::class,'latest']);
    Route::get('/{id}', [CovidFormController::class,'show']);
    Route::post('/',[CovidFormController::class,'store']);
    Route::put('/{id}',[CovidFormController::class,'update']);
    Route::delete('/{id}',[CovidFormController::class,'destroy']);
    Route::get('/download/{file}',[CovidFormController::class,'download']);
    // Route::get('/export_data',[CovidFormController::class,'export_data']);
});
Route::get('/export_data/{location}',[CovidFormController::class,'export_data']);

//  Trainings Forms
Route::group(['prefix' => 'training_forms', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [TrainingFormController::class,'index']);
    Route::get('/latest', [TrainingFormController::class,'latest']);
    Route::get('/{id}', [TrainingFormController::class,'show']);
    Route::post('/',[TrainingFormController::class,'store']);
    Route::put('/{id}',[TrainingFormController::class,'update']);
    Route::delete('/{id}',[TrainingFormController::class,'destroy']);
    Route::GET('/download/{file}',[TrainingFormController::class,'download']);
});
Route::get('/search_training_form', [TrainingFormController::class,'search_training_form_by_key']);

//  Form
Route::group(['prefix' => 'forms', 'middleware' => 'auth:sanctum'], function() {  
    Route::get('/', [FormController::class,'index']);
    Route::get('/latest', [FormController::class,'latest']);
    Route::get('/{id}', [FormController::class,'show']);
    Route::post('/',[FormController::class,'store']);
    Route::put('/{id}',[FormController::class,'update']);
    Route::delete('/{id}',[FormController::class,'destroy']);
    Route::GET('/download/{file}',[FormController::class,'download']);
});
Route::get('/search_form', [FormController::class,'search_form_by_key']);


//  Form
Route::group(['prefix' => 'file_managers', 'middleware' => 'auth:sanctum'], function() {  
    Route::get('/', [FileManagerController::class,'index']);
    Route::get('/latest', [FileManagerController::class,'latest']);
    Route::get('/{id}', [FileManagerController::class,'show']);
    Route::post('/',[FileManagerController::class,'store']);
    Route::put('/{id}',[FileManagerController::class,'update']);
    Route::delete('/{id}',[FileManagerController::class,'destroy']);
    Route::GET('/download/{file}',[FileManagerController::class,'download']);
});

//  Hr Docs
Route::prefix('hr_docs')->group(function () {    
    Route::get('/', [HrDocController::class,'index']);
    Route::get('/latest', [HrDocController::class,'latest']);
    Route::get('/{id}', [HrDocController::class,'show']);
    Route::post('/',[HrDocController::class,'store']);
    Route::put('/{id}',[HrDocController::class,'update']);
    Route::delete('/{id}',[HrDocController::class,'destroy']);
    Route::GET('/download/{file}',[HrDocController::class,'download']);
});
Route::get('/search_hr_doc', [HrDocController::class,'search_hr_doc_by_key']);


//  Tab Description
Route::prefix('tab_descriptions')->group(function () {    
    Route::get('/', [TabDescriptionController::class,'index']);
    Route::get('/{id}', [TabDescriptionController::class,'show']);
    Route::put('/{id}',[TabDescriptionController::class,'update']);
});


//  Scholarship
Route::prefix('scholarships')->group(function () {    
    Route::get('/', [ScholarshipController::class,'index']);
    Route::get('/latest', [ScholarshipController::class,'latest']);
    Route::get('/{id}', [ScholarshipController::class,'show']);
    Route::post('/',[ScholarshipController::class,'store']);
    Route::put('/{id}',[ScholarshipController::class,'update']);
    Route::delete('/{id}',[ScholarshipController::class,'destroy']);
    Route::GET('/download/{file}',[ScholarshipController::class,'download']);
});
Route::get('/search_scholarship', [ScholarshipController::class,'search_scholarship_by_key']);


//  Creatives
Route::group(['prefix' => 'creatives', 'middleware' => 'auth:sanctum'], function() {       
    Route::get('/', [CreativeController::class,'index']);
    Route::get('/{id}', [CreativeController::class,'show']);
    Route::post('/',[CreativeController::class,'store']);
    Route::put('/{id}',[CreativeController::class,'update']);
    Route::delete('/{id}',[CreativeController::class,'destroy']);
    Route::get('/download/{file}',[CreativeController::class,'download']);

});
Route::get('/search_creative', [CreativeController::class,'search_creative_by_key']);

//  Uniform
Route::group(['prefix' => 'uniforms', 'middleware' => 'auth:sanctum'], function() {            
    Route::get('/', [UniformController::class,'index']);
    Route::get('/latest', [UniformController::class,'latest']);
    Route::get('/{id}', [UniformController::class,'show']);
    Route::post('/',[UniformController::class,'store']);
    Route::put('/{id}',[UniformController::class,'update']);
    Route::delete('/{id}',[UniformController::class,'destroy']);
    Route::GET('/download/{file}',[UniformController::class,'download']);
});
Route::get('/search_uniform', [UniformController::class,'search_uniform_by_key']);

// Events
Route::group(['prefix' => 'events', 'middleware' => 'auth:sanctum'], function() {        
    Route::get('/', [EventController::class,'index']);
    Route::get('/{id}', [EventController::class,'employee_pto']);
    Route::get('/{id}', [EventController::class,'show']);
    Route::get('/emp/{user_id}', [EventController::class,'get_pto']);
    Route::post('/',[EventController::class,'store']);
    Route::put('/{id}',[EventController::class,'update']);
    Route::delete('/{id}',[EventController::class,'destroy']);    
});

Route::get('/active_users', [ActivityController::class,'monthly_active_users']);
Route::get('/average_users', [ActivityController::class,'getClickUserAverage']);
Route::get('/active_usertypes', [ActivityController::class,'monthly_active_usertypes']);
Route::get('/hamburg_metrics', [ActivityController::class,'monthly_hamburg_views']);
Route::get('/richmond_metrics', [ActivityController::class,'monthly_richmond_views']);  

// Activity Notifications
Route::group(['prefix' => 'notif_activities', 'middleware' => 'auth:sanctum'], function() {

    Route::get('/', [ActivityController::class,'index']);
    
    Route::get('/{id}', [ActivityController::class,'employee_pto']);
    Route::get('/{id}', [ActivityController::class,'show']);
    Route::post('/',[ActivityController::class,'store']);
    Route::put('/{id}',[ActivityController::class,'update']);
    Route::delete('/{id}',[ActivityController::class,'destroy']);    

    Route::put('/{id}/mark_read',[ActivityController::class,'mark_read']);
    Route::post('/mark_all_read',[ActivityController::class,'mark_all_read']);
});



// Test Email
Route::get('testemail', function () {
    Mail::to(['adrian@ad-ios.com'])->send(new EventMailer);
});
Route::prefix('channels')->group(function () {    
    Route::get('/', [ChannelController::class,'index']);
});

// Threads
Route::group(['prefix' => 'threads', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [ThreadController::class,'index']);
    Route::get('/single_news', [ThreadController::class,'single_news']);
    Route::get('/featured', [ThreadController::class,'featured']);
    Route::post('/', [ThreadController::class,'store']);      
    Route::get('/get/{thread}', [ThreadController::class,'show']);
    Route::put('/{thread}',[ThreadController::class,'update']);
    Route::delete('/{thread}', [ThreadController::class,'destroy']);
    Route::post('/{thread}/replies', [ThreadController::class,'store']);
    Route::post('/{thread}/replies', [ReplyController::class,'store']);
    Route::put('/{thread}/favorites',[ThreadController::class,'favorited']);
    Route::put('/{thread}/unfavorites',[ThreadController::class,'unfavorited']);
    Route::post('/reacts',[ThreadController::class,'reacted']);   
    Route::delete('/unreact/{thread}', [ThreadController::class,'unreacted']);
    Route::get('/{thread}/replies', [ReplyController::class,'index']);
    Route::get('/emp/{thread}/replies', [ReplyController::class,'get_visible_replies']);
    Route::get('/update_counter',[ThreadController::class,'update_counter']);
});

// Route::get('/search_news', [ThreadController::class,'search_news_by_key']);


// Hr Forms
Route::prefix('hr_forms')->group(function () {    
    Route::get('/', [HrFormController::class,'index']);    
    Route::post('/', [HrFormController::class,'store']);      
    Route::get('/{hr_form}', [HrFormController::class,'show']);
    Route::put('/{hr_form}',[HrFormController::class,'update']);
    Route::delete('/{hr_form}', [HrFormController::class,'destroy']);
    Route::post('/{hr_form}/replies', [HrFormController::class,'store']);
    Route::post('/{hr_form}/replies', [HrReplyController::class,'store']);
    Route::put('/{hr_form}/favorites',[HrFormController::class,'favorited']);
    Route::put('/{hr_form}/unfavorites',[HrFormController::class,'unfavorited']);
    Route::get('/{hr_form}/replies', [HrReplyController::class,'index']);
    Route::get('/emp/{hr_form}/replies', [HrReplyController::class,'get_visible_replies']);
    Route::get('/emp/{user_id}', [HrFormController::class,'get_hr_form']);
    Route::get('/{id}', [HrFormController::class,'user_hr_form']);
    
});
Route::get('/search_hr_forms', [HrFormController::class,'search_hr_forms_by_key']);


// Facilities
Route::group(['prefix' => 'facilities', 'middleware' => 'auth:sanctum'], function() {       
    Route::get('/', [FacilityController::class,'index']);    
    Route::post('/', [FacilityController::class,'store']);      
    Route::get('/{form}', [FacilityController::class,'show']);
    Route::put('/{form}',[FacilityController::class,'update']);
    Route::delete('/{form}', [FacilityController::class,'destroy']);
    Route::post('/{form}/replies', [FacilityController::class,'store']);
    Route::post('/{form}/replies', [FacilityReplyController::class,'store']);
    Route::put('/{form}/favorites',[FacilityController::class,'favorited']);
    Route::put('/{form}/unfavorites',[FacilityController::class,'unfavorited']);
    Route::get('/{form}/replies', [FacilityReplyController::class,'index']);
    Route::get('/emp/{form}/replies', [FacilityReplyController::class,'get_visible_replies']);
    Route::get('/emp/{user_id}', [FacilityController::class,'get_hr_form']);
    Route::get('/{id}', [FacilityController::class,'user_hr_form']);
    
});

// Facility Replies
Route::group(['prefix' => 'facility_replies', 'middleware' => 'auth:sanctum'], function() {         
    Route::get('/{id}', [FacilityReplyController::class,'show']);       
    Route::put('/{id}',[FacilityReplyController::class,'update']);
    Route::delete('/{id}',[FacilityReplyController::class,'destroy']);        
});


// Group Message
Route::group(['prefix' => 'group_messages', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/', [GroupMessageController::class,'index']);    
    Route::post('/', [GroupMessageController::class,'store']);
    Route::post('/send_multiple_users', [GroupMessageController::class,'sendMultipleUsers']);
    Route::get('/{group_message}', [GroupMessageController::class,'show']);
    Route::put('/{group_message}',[GroupMessageController::class,'update']);
    Route::delete('/bulk_delete/{group_message}', [GroupMessageController::class,'bulkDelete']);
    Route::delete('/{group_message}', [GroupMessageController::class,'destroy']);
    Route::post('/{group_message}/replies', [GroupMessageController::class,'store']);
    Route::post('/{group_message}/replies', [GroupMessageReplyController::class,'store']);
    Route::put('/{group_message}/favorites',[GroupMessageController::class,'favorited']);
    Route::put('/{group_message}/unfavorites',[GroupMessageController::class,'unfavorited']);
    Route::get('/{group_message}/replies', [GroupMessageReplyController::class,'index']);
    Route::get('/emp/{group_message}/replies', [GroupMessageReplyController::class,'get_visible_replies']);
    Route::get('/emp/{user_id}', [GroupMessageController::class,'get_group_message']);
    Route::get('/{id}', [GroupMessageController::class,'user_group_message']);
    // Route::get('/search_group_messages', [GroupMessageController::class,'search_group_message_by_key']);    
});


// Group Message Replies
Route::prefix('group_message_replies')->group(function () {     
    
    Route::get('/{id}', [GroupMessageReplyController::class,'show']);       
    Route::put('/{id}',[GroupMessageReplyController::class,'update']);
    Route::delete('/{id}',[GroupMessageReplyController::class,'destroy']);        
});


// Replies
Route::group(['prefix' => 'replies', 'middleware' => 'auth:sanctum'], function() {
    Route::get('/{id}', [ReplyController::class,'show']);       
    Route::put('/{id}',[ReplyController::class,'update']);
    Route::delete('/{id}',[ReplyController::class,'destroy']);    
    Route::delete('/{id}/favorites',[FavoritesController::class,'destroy']);    
    Route::delete('/{id}/favorites',[ReplyController::class,'store']);    

    Route::post('/reacts',[ReplyController::class,'reacted']);    
    Route::delete('/unreact/{thread}', [ReplyController::class,'unreacted']);


});

// Hr Replies
Route::group(['prefix' => 'hr_replies', 'middleware' => 'auth:sanctum'], function() {    
    Route::get('/{id}', [HrReplyController::class,'show']);       
    Route::put('/{id}',[HrReplyController::class,'update']);
    Route::delete('/{id}',[HrReplyController::class,'destroy']);        
});


// Mark as Read Notification
Route::get('/markAsRead', function(){

	auth()->user()->unreadNotifications->markAsRead();

	return redirect()->back();

})->name('mark');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/set-password',[AuthController::class,'set_password']);
Route::group(['prefix' => 'users','middleware' => ['auth:sanctum']], function () {
    Route::post('/',[AuthController::class,'store']);
    Route::put('/{id}',[AuthController::class,'update']);
    Route::put('/update/{id}',[AuthController::class,'update_user']);
    Route::delete('/{id}',[AuthController::class,'destroy']);
    Route::post('/auth/logout', [AuthController::class, 'signout']); 
    Route::post('/change_password', [AuthController::class, 'change_password']); 
    Route::get('/resend_set_password/{id}',[AuthController::class,'resend_set_password']);
 });

